
Readme
======

Проект сети магазинов

:Authors: Мы

:Version: 0.1

**********************
Разворачивание проекта
**********************


Создать виртуальную среду.
--------------------------
Зайти в папку проекта src и выполнить::

    virtualenv .venv

потом активируем::

    . .venv/bin/activate

Установить сразу все нужные модули для разработки и тестирования::

    pip install -r docs/develop.txt

для продакшена просто::

    pip install -r docs/requirements.txt


Создание базы данных в Postgres
-------------------------------

Выполняем в консоли ::

    NAME_DB=fandeco
    PASSWORD_DB=fandeco
    sudo -u postgres psql -q -c "create database $NAME_DB with encoding='UNICODE'"
    sudo -u postgres psql -q -c "create user $NAME_DB with password '$PASSWORD_DB'"
    sudo -u postgres psql -q -c "grant all privileges on database $NAME_DB to $NAME_DB"
    sudo -u postgres psql -q -c "alter database $NAME_DB owner to $NAME_DB"

** первые переменные по желанию изменяйте на свои, потом прописать их в настройках джанги

Для тестов в Django так же можно дать права пользователю на создание базы с префиксом ``test_``::

    sudo -u postgres psql -q -c "alter user $NAME_DB CREATEDB;"


Тестирование
------------
Запуск обычный или с подробным информированием::

    ./manage.py test --nomigrations
    ./manage.py test -v2  --nomigrations