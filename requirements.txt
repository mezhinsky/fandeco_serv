Django==1.8.4
django-autoslug==1.8.0
django-debug-toolbar==1.3.2
django-extensions==1.5.7
django-flat-theme==1.1.1

six==1.9.0
sqlparse==0.1.16
wsgiref==0.1.2
