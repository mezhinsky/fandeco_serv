# -*- coding: utf-8 -*-

from django.db import models


class UrlData(models.Model):

    url = models.CharField(
        u'url', max_length=255, primary_key=True, unique=True,
        help_text=u'формат ввода url: "/ru/projects/"')
    title = models.CharField(u'title', max_length=128, blank=True, null=True)
    keywords = models.TextField('keywords', max_length=600, blank=True, null=True)
    description = models.TextField(u'description', max_length=600, blank=True, null=True)

    class Meta:
        verbose_name = u'Url данные'
        verbose_name_plural = u'Url данные'

    def __unicode__(self):
        return u'"%s" -> %s' % (self.url, self.title)
