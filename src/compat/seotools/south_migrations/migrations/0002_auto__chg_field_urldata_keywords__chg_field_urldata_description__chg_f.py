# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):

        # Changing field 'UrlData.keywords'
        db.alter_column(u'seotools_urldata', 'keywords', self.gf('django.db.models.fields.TextField')(max_length=600, null=True))

        # Changing field 'UrlData.description'
        db.alter_column(u'seotools_urldata', 'description', self.gf('django.db.models.fields.TextField')(max_length=600, null=True))

        # Changing field 'UrlData.title'
        db.alter_column(u'seotools_urldata', 'title', self.gf('django.db.models.fields.CharField')(max_length=128, null=True))

    def backwards(self, orm):

        # User chose to not deal with backwards NULL issues for 'UrlData.keywords'
        raise RuntimeError("Cannot reverse this migration. 'UrlData.keywords' and its values cannot be restored.")
        
        # The following code is provided here to aid in writing a correct migration
        # Changing field 'UrlData.keywords'
        db.alter_column(u'seotools_urldata', 'keywords', self.gf('django.db.models.fields.TextField')(max_length=600))

        # User chose to not deal with backwards NULL issues for 'UrlData.description'
        raise RuntimeError("Cannot reverse this migration. 'UrlData.description' and its values cannot be restored.")
        
        # The following code is provided here to aid in writing a correct migration
        # Changing field 'UrlData.description'
        db.alter_column(u'seotools_urldata', 'description', self.gf('django.db.models.fields.TextField')(max_length=600))

        # User chose to not deal with backwards NULL issues for 'UrlData.title'
        raise RuntimeError("Cannot reverse this migration. 'UrlData.title' and its values cannot be restored.")
        
        # The following code is provided here to aid in writing a correct migration
        # Changing field 'UrlData.title'
        db.alter_column(u'seotools_urldata', 'title', self.gf('django.db.models.fields.CharField')(max_length=128))

    models = {
        u'seotools.urldata': {
            'Meta': {'object_name': 'UrlData'},
            'description': ('django.db.models.fields.TextField', [], {'max_length': '600', 'null': 'True', 'blank': 'True'}),
            'keywords': ('django.db.models.fields.TextField', [], {'max_length': '600', 'null': 'True', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '128', 'null': 'True', 'blank': 'True'}),
            'url': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '255', 'primary_key': 'True'})
        }
    }

    complete_apps = ['seotools']