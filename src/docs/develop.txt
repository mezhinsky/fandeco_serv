
-r requirements.txt
-r testing.txt

django_debug_toolbar>=1.3.2
django-debug-toolbar-template-timings
django-extensions==1.5.6
django_rosetta>=0.7.6
