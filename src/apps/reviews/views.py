import json

from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.contrib.contenttypes.models import ContentType
from django.contrib.sites.models import Site
from django.template.loader import render_to_string
from django.http.response import Http404, HttpResponse
from django.shortcuts import get_object_or_404

from reviews.forms import ReviewTextForm
from reviews.settings import r_settings
from reviews.signals import review_added
from reviews.models import Review, Vote
from django.core.mail import send_mail


@login_required
def add_review(request, **kwargs):
    if not request.is_ajax():
        raise Http404
    is_valid = False
    html_review = None
    template = 'reviews/ajax/{0}'
    form_class = ReviewTextForm
    try:
        pk = int(request.POST.get('content_id', None))
        model_pk = int(request.POST.get('content_type', None))
    except:
        raise Http404
    model = get_object_or_404(ContentType, pk=model_pk)
    form = form_class(request.POST)
    if form.is_valid():
        form.instance.content_type = model
        form.instance.object_id = pk
        form.instance.user = request.user
        try:
            score = int(request.POST.get('score', None))
        except:
            score = 0
        form.instance.score = score
        review = form.save(commit=True)
        review_added.send(sender=form, review=review, user=request.user,
                              request=request)
        context = {'review':review,
                    'site': Site.objects.get_current()}
        send_mail(
                  template_name='admin_review',
                  from_email=settings.DEFAULT_FROM_EMAIL,
                  recipient_list=[item[1] for item in settings.MANAGERS],
                  context=context,
                  )
        if not r_settings.MODERATE_REQUIRE:
            html_review = render_to_string(
                'reviews/includes/reviews_items_list.html',
                {'review': review,
                 'request':request}
                )
        form = form_class()
        is_valid = True
    html = render_to_string(
                                template.format('reviews_form.html'),
                                {'form': form,
                                 'request':request}
                        )
    json_dump = json.dumps({'html': html, 'html_review':html_review, 'is_valid':is_valid, 'is_moderator':r_settings.MODERATE_REQUIRE})
    return HttpResponse(json_dump, status=200, mimetype='application/json')


@login_required
def vote_reviews(request, **kwargs):
    status = request.GET.get('status', None)
    if status == 'like' or status == 'dislike' and request.is_ajax():
        review = get_object_or_404(Review.approved.all(), pk=kwargs['pk'])
        urv = Vote.objects.filter(review=review, user=request.user, delta=1)
        drv = Vote.objects.filter(review=review, user=request.user, delta= -1)
        if status == 'like':
            if drv:
                drv.delete()
            if not urv:
                Vote.objects.create(review=review, user=request.user, delta=1)
        else:
            if urv:
                urv.delete()
            if not drv:
                Vote.objects.create(review=review, user=request.user, delta= -1)
        count_votes_up = Vote.objects.filter(review=review, delta=1).count()
        count_votes_down = Vote.objects.filter(review=review, delta= -1).count()
        review.count_votes_up = count_votes_up
        review.count_votes_down = count_votes_down
        review.save(update_fields=['count_votes_up', 'count_votes_down'])
        json_dump = json.dumps({'count_votes_down': count_votes_down, 'count_votes_up':count_votes_up, 'pk':kwargs['pk']})
        return HttpResponse(json_dump, status=200, mimetype='application/json')
    else:
        raise Http404

#
# def Entry_update_counter_reviews(sender, **kwargs):
#     review = kwargs['review']
#     content_type = review.content_type
#     try:
#         entry = content_type.model_class().objects.get(pk=review.object_id)
#     except:
#         entry = False
#     if entry:
#         entry.count_reviews = Review.approved.filter(content_type=content_type, object_id=review.object_id).count()
#         ar = Review.approved.filter(content_type=content_type, object_id=review.object_id, score__gt=0).aggregate(Avg('score'))
#         if ar['score__avg']:
#             entry.rating = ar['score__avg']
#             entry.save(update_fields=['count_reviews', 'rating'])
#         else:
#             entry.save(update_fields=['count_reviews'])
#
# review_added.connect(Entry_update_counter_reviews)
