$(document).ready(function(){
    var form = $("#reviews-form");
    form.submit(function(e) {
    	$(".fon-ajax-loader").show();
  		$(".image-ajax-loader").show();
        $.post(
            form.attr('action'),
            form.serialize(),
            function(data) {
            	$('.change_form').html(data.html);
            	if (data.is_moderator==false){
            		$('.block_reviews').prepend(data.html_review);
            	}
            	if (data.is_valid){
            		$(".ratings_selected").css("width", "0");
            		$("#id_score").val(0);
            		$('#add_reviews').foundation('reveal', 'open');
            	}
                $(document).foundation('alerts dropdown forms');
                $(".fon-ajax-loader").hide();
			  	$(".image-ajax-loader").hide();
            }, "json");
        
        e.preventDefault();
    });
    
    $(".ratings_part").click(function(){
    	var ratings = $(this).attr('value');
    	var width_selected = 17*ratings;
    	$(".ratings_selected").css("width", width_selected+"px");
    	$("#id_score").val(ratings);
	});
});

function VoteReviews(url, status){
		$.ajax({
		    type: 'GET',
		    url: url,
		    data: {status:status},
		    success: function(data) {
		    	$('.like_'+data.pk).html('<i class="fi-like"></i>'+data.count_votes_up);
		    	$('.dislike_'+data.pk).html('<i class="fi-dislike"></i>'+data.count_votes_down);
		  	},
		 	beforeSend : function(){
			  	$(".fon-ajax-loader").show();
			  	$(".image-ajax-loader").show();
			},
	     	complete:function(){
			  	$(".fon-ajax-loader").hide();
			  	$(".image-ajax-loader").hide();
			},
		});
};