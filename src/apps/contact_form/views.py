"""
View which can render and send email from a contact form.

"""
import json

from django.contrib.auth.views import redirect_to_login
from django.http.response import Http404, HttpResponseRedirect, HttpResponse,\
    JsonResponse
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.template.loader import render_to_string

from contact_form.forms import ContactFormCaptcha


def contact_form(request, form_class=ContactFormCaptcha, template_name='contact_form/contact_form.html', success_url='/contact/sent/', login_required=False, fail_silently=False):
    """
    Renders a contact form, validates its input and sends an email
    from it.

    To specify the form class to use, pass the ``form_class`` keyword
    argument; if no ``form_class`` is specified, the base
    ``ContactForm`` class will be used.

    To specify the template to use for rendering the form (*not* the
    template used to render the email message sent from the form,
    which is handled by the form class), pass the ``template_name``
    keyword argument; if not supplied, this will default to
    ``contact_form/contact_form.html``.

    To specify a URL to redirect to after a successfully-sent message,
    pass the ``success_url`` keyword argument; if not supplied, this
    will default to ``/contact/sent/``.

    To allow only registered users to use the form, pass a ``True``
    value for the ``login_required`` keyword argument.

    To suppress exceptions raised during sending of the email, pass a
    ``True`` value for the ``fail_silently`` keyword argument. This is
    **not** recommended.

    Template::

        Passed in the ``template_name`` argument.

    Context::

        form
            The form instance.

    """
    if login_required and not request.user.is_authenticated():
        return redirect_to_login(request.path)

    if request.method == 'POST':
        form = form_class(request.POST, request=request)
        if form.is_valid():
            form.save(fail_silently=fail_silently)
            return HttpResponseRedirect(success_url)
    else:
        form = form_class(request=request)
    return render_to_response(template_name,
                              { 'form': form },
                              context_instance=RequestContext(request))


def contact_tags_form(request):
    if not request.is_ajax():
        raise Http404    
    template = 'contact_form/{0}'
    is_valid = None
    if request.method == 'POST':
        form = ContactFormCaptcha(request.POST, request=request)
        is_valid = form.is_valid()
        if is_valid:
            form.save()
    else:
        form = ContactFormCaptcha()
    
    if is_valid:
        html = render_to_string(template.format('contact_form_ajax_sent.html'), 
                RequestContext(request, {'form': form}) )
    else:
        html = render_to_string(template.format('contact_form_ajax.html'), 
                RequestContext(request, {'form': form}) )

    return JsonResponse({'html': html, 'is_valid': is_valid})        