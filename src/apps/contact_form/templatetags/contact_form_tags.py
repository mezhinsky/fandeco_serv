
from django import template

from contact_form.forms import ContactFormCaptcha

register = template.Library()


@register.inclusion_tag('contact_form/contact_form_ajax.html', takes_context=True)
def show_contact_form(context):
    form=ContactFormCaptcha(request=context["request"])
    context.update({'form': form})
    return context
