# -*- coding: utf-8 -*-

import json

from django.conf import settings
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.sites.models import Site
from django.core.mail import send_mail
from django.core.urlresolvers import reverse_lazy
from django.http import HttpResponseRedirect
from django.http.response import HttpResponse
from django.shortcuts import get_object_or_404, redirect
from django.template.defaultfilters import striptags
from django.template.loader import render_to_string
from django.utils.translation import ugettext as _
from django.views.generic.detail import DetailView
from django.views.generic.edit import FormView
from django.views.generic.list import ListView

from braces.views import LoginRequiredMixin

from overviews.forms import OverviewPostForm
from overviews.models import Overview


class AddOverviewView(LoginRequiredMixin , FormView):
    form_class = OverviewPostForm
    template_name = 'overviews/post_add.html'

    def dispatch(self, request, *args, **kwargs):
        self.entry = get_object_or_404(Entry, pk=self.kwargs['entry_pk'])
        return super(AddOverviewView, self).dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        form.instance.author = self.request.user
        form.instance.entry = self.entry
        form.instance.description = striptags(form.instance.body_html)[:240]
        obj = form.save(commit=True)
        return HttpResponseRedirect(obj.get_absolute_preview_url())

    def form_invalid(self, form):
        messages.error(
            self.request,
            _('Please correct the errors below and try again')
        )
        return super(AddOverviewView, self).form_invalid(form)

    def get_context_data(self, **kwargs):
        context = super(AddOverviewView, self).get_context_data(**kwargs)
        context['object'] = self.entry
        return context


class EditOverviewView(LoginRequiredMixin, FormView):
    form_class = OverviewPostForm
    template_name = 'overviews/post_add.html'

    def get_initial(self):
        initial = {}
        self.overview = get_object_or_404(Overview, author=self.request.user, pk=self.kwargs['pk'])
        for field in self.overview._meta.get_all_field_names():
            initial[field] = self.overview.__getattribute__(field)
        return initial

    def form_valid(self, form):
        form.instance.id = self.overview.id
        form.instance.created_at = self.overview.created_at
        form.instance.author = self.request.user
        form.instance.entry = self.overview.entry
        form.instance.description = striptags(form.instance.body_html)[:240]
        obj = form.save(commit=True)
        return HttpResponseRedirect(obj.get_absolute_preview_url())

    def form_invalid(self, form):
        messages.error(
            self.request,
            _('Please correct the errors below and try again')
        )
        return super(EditOverviewView, self).form_invalid(form)

    def get_context_data(self, **kwargs):
        context = super(EditOverviewView, self).get_context_data(**kwargs)
        context['object'] = self.overview.entry
        context['edit'] = True
        return context


class MyOverviewListView(LoginRequiredMixin, ListView):
    template_name = 'overviews/my_list.html'
    context_object_name = 'overviews_list'
    per_page = 10
    filter_title = 'All'

    def get_queryset(self):
        qkw = {}
        published = self.request.GET.get('published', None)
        moderated = self.request.GET.get('moderated', None)
        draft = self.request.GET.get('draft', None)
        if published == '1':
            self.filter_title = 'Published'
            qkw['status'] = Overview.PUBLISHED
        elif moderated == '1':
            self.filter_title = 'Moderated'
            qkw['status'] = Overview.MODERATED
        if draft == '1':
            self.filter_title = 'Draft'
            qkw['status'] = Overview.DRAFT
        return Overview.objects.filter(is_deleted=False, author=self.request.user, **qkw).select_related('entry')

    def get_context_data(self, **kwargs):
        context = super(MyOverviewListView, self).get_context_data(**kwargs)
        context['paginator'] = self.request.GET.get('set_per_page', None)
        context['filter_title'] = self.filter_title
        return context


class OverviewPreviewDetailView(LoginRequiredMixin, DetailView):
    model = Overview
    template_name = 'overviews/post_preview_detail.html'

    def get_object(self):
        if self.request.user.is_staff:
            obj = get_object_or_404(Overview.objects.exclude(status=Overview.PUBLISHED), pk=self.kwargs['pk'])
        else:
            obj = get_object_or_404(Overview.objects.exclude(status=Overview.PUBLISHED, is_deleted=True), author=self.request.user, pk=self.kwargs['pk'])
        return obj


class OverviewDetailView(DetailView):
    model = Overview
    template_name = 'overviews/post_detail.html'

    def get_object(self):
        obj = get_object_or_404(Overview.objects.all_published(),
                                slug=self.kwargs['slug'], pk=self.kwargs['pk'])
        return obj


@login_required
def delete_overview(request, **kwargs):
    review = get_object_or_404(Overview, pk=kwargs['pk'], author=request.user, is_deleted=False, status=DRAFT)
    Overview.objects.filter(id=review.id).update(is_deleted=True)
    return redirect(reverse_lazy('overviews_my'))


@login_required
def moderated_overview(request, **kwargs):
    overview = get_object_or_404(Overview, pk=kwargs['pk'],
                                 author=request.user, status=Overview.DRAFT)
    overview.status = Overview.MODERATED
    overview.save()
    context = {'overview':overview,
               'site': Site.objects.get_current()}
    send_mail(
              template_name='admin_overview',
              from_email=settings.DEFAULT_FROM_EMAIL,
              recipient_list=[item[1] for item in settings.MANAGERS],
              context=context,
              )
    return redirect(reverse_lazy('overviews_my'))


@login_required
def moderated_overview_popup(request, **kwargs):
    template = 'overviews/ajax/{0}'
    html = render_to_string(
            template.format('moderated_overview.html'),
            {'pk':kwargs['pk']}
            )
    json_dump = json.dumps({'html': html})
    return HttpResponse(json_dump, status=200, mimetype='application/json')



@login_required
def delete_overview_popup(request, **kwargs):
    template = 'overviews/ajax/{0}'
    html = render_to_string(
            template.format('delete_overview.html'),
            {'pk':kwargs['pk']}
            )
    json_dump = json.dumps({'html': html})
    return HttpResponse(json_dump, status=200, mimetype='application/json')
