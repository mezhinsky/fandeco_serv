# -*- coding: utf-8 -*-

from django.contrib import admin

from overviews.models import Overview
from corelib.admin import DeleteWithSignalMixin


@admin.register(Overview)
class OverviewAdmin(DeleteWithSignalMixin, admin.ModelAdmin):
    list_display = ('id', 'title', 'author', 'entry', 'published_at', 'status', 'is_deleted',)
    list_display_links = ('title',)
    list_filter = ('status',)
    search_fields = ('=id', 'title', '=slug')
    prepopulated_fields = {'slug': ('title',)}
