# -*- coding: utf-8 -*-

from django.db import models
from django.utils import timezone


class OverviewPublishedManager(models.Manager):

    def all_published(self):
        now = timezone.now()
        return super(OverviewPublishedManager, self).get_queryset().filter(
                    author__is_active=True, status=self.model.PUBLISHED,
                    is_deleted=False, published_at__lte=now,
                    ).order_by("-published_at")
