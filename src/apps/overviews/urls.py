# -*- coding: utf-8 -*-

from django.conf.urls import url, patterns
from overviews.views import MyOverviewListView, AddOverviewView, EditOverviewView, \
    delete_overview_popup, delete_overview, moderated_overview, moderated_overview_popup, \
    OverviewPreviewDetailView, OverviewDetailView


urlpatterns = patterns("",
    url(r'^my/$', MyOverviewListView.as_view(), name='overviews_my',),
    url(r'^(?P<entry_pk>\d+)/add/$', AddOverviewView.as_view(), name='overviews_add',),
    url(r'^(?P<pk>\d+)/edit/$', EditOverviewView.as_view(), name='overviews_edit',),
    url(r'^(?P<pk>\d+)/delete/popup/$', delete_overview_popup, name='overviews_delete_popup',),
    url(r'^(?P<pk>\d+)/delete/overview/$', delete_overview, name='overviews_delete',),
    url(r'^(?P<pk>\d+)/moderated/overview/$', moderated_overview, name='overviews_moderated',),
    url(r'^(?P<pk>\d+)/moderated/popup/$', moderated_overview_popup, name='overviews_moderated_popup',),
    url(r'^(?P<pk>\d+)/preview/$', OverviewPreviewDetailView.as_view(), name='overviews_preview_detail',),
    url(r'^(?P<slug>[\w-]+)-(?P<pk>\d+)/$', OverviewDetailView.as_view(), name='overviews_detail',),
)
