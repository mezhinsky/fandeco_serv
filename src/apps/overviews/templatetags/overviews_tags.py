# -*- coding: utf-8 -*-

from django import template
from overviews.models import Overview

register = template.Library()


@register.inclusion_tag('overviews/tags/overviews_home_widget.html', takes_context=True)
def get_overviews_on_home(context, count=5):
    overviews = Overview.published.all().order_by('-published_at')[:count]
    return {'overview_list':overviews, 'request':context['request'], }


@register.inclusion_tag('overviews/tags/overviews_list.html', takes_context=True)
def get_overviews_list(context, obj):
    overviews = Overview.published.filter(entry=obj)
    return {
            'overviews':overviews,
            'request':context['request'], 'object':obj
            }
