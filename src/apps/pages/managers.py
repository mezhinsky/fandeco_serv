# -*- coding: utf-8 -*-
from django.db import models
from django.utils import timezone


class PageManager(models.Manager):

    def all_published(self):
        now = timezone.now()
        qs = super(PageManager, self).get_queryset()
        return qs.filter(is_published=True, published_at__lte=now)
