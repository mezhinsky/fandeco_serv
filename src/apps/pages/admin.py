# -*- coding: utf-8 -*-

from django.contrib import admin

from ckeditor.widgets import CKEditorWidget

try:
    from modeltranslation.admin import TranslationAdmin
except:
    from django.contrib.admin.options import ModelAdmin as TranslationAdmin

from .models import Page


@admin.register(Page)
class PageAdmin(TranslationAdmin):
    list_display = ('title', 'slug', 'is_published', 'is_in_sitemap', )
    list_filter = ('is_published', 'is_in_sitemap')
    search_fields = ['slug', 'title']
    prepopulated_fields = {'slug': ('title',)}

    class Media:
        js = (
            '/static/modeltranslation/js/force_jquery.js',
            'http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.2/jquery-ui.min.js',
            '/static/modeltranslation/js/tabbed_translation_fields.js',
        )
        css = {
            'screen': ('/static/modeltranslation/css/tabbed_translation_fields.css',),
        }

    def formfield_for_dbfield(self, db_field, **kwargs):
        if db_field.name in ('body_html',):
            return db_field.formfield(widget=CKEditorWidget(config_name='admin_ckeditor'))
        return super(PageAdmin, self).formfield_for_dbfield(db_field, **kwargs)

