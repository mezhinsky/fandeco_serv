# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Page',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=254, verbose_name='title')),
                ('slug', models.SlugField(unique=True, max_length=128, verbose_name='slug')),
                ('body_html', models.TextField(verbose_name='HTML version')),
                ('is_published', models.BooleanField(default=False, verbose_name='published')),
                ('published_at', models.DateTimeField(default=django.utils.timezone.now, verbose_name='published at')),
                ('is_in_sitemap', models.BooleanField(default=True, verbose_name='in sitemap')),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='created at')),
                ('updated_at', models.DateTimeField(auto_now=True, verbose_name='updated at')),
            ],
            options={
                'ordering': ['-title'],
                'verbose_name': 'Page',
                'verbose_name_plural': 'Pages',
            },
        ),
    ]
