# -*- coding: utf-8 -*-
from django.conf.urls import url, patterns
from . import views

# namespace='pmdocs'
urlpatterns = patterns('',
    url(r'^$', views.PageListView.as_view(), name='pages_page_list',),
    url(r'^(?P<slug>[-\w]+)/$', views.PageDetailView.as_view(), name='pages_page_detail',),
)
