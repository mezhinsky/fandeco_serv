# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.core.urlresolvers import reverse
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _

from pages.managers import PageManager

class Page(models.Model):
    title = models.CharField(_('title'), max_length=254)
    slug = models.SlugField(_("slug"), max_length=128, unique=True)
    body_html = models.TextField(_('HTML version'))

    is_published = models.BooleanField(_('published'), default=False)
    published_at = models.DateTimeField(_("published at"), default=timezone.now)
    is_in_sitemap = models.BooleanField(_('in sitemap'), default=True)

    created_at = models.DateTimeField(_('created at'), auto_now_add=True)
    updated_at = models.DateTimeField(_('updated at'), auto_now=True)

    objects = PageManager()

    class Meta:
        ordering = ['-title']
        verbose_name = _("Page")
        verbose_name_plural = _("Pages")

    def __unicode__(self):
        return u'%s [ %s ]' % (self.slug, self.title,)

    def get_absolute_url(self):
        return reverse('pages_page_detail', kwargs={'slug': self.slug})
