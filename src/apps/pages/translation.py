# -*- coding: utf-8 -*-

from modeltranslation.translator import translator, TranslationOptions
from .models import Page

class PageTranslation(TranslationOptions):
	fields = ('title', 'body_html',)

translator.register(Page, PageTranslation)

