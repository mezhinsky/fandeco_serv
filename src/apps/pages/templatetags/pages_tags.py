# -*- coding: utf-8 -*-
from django import template
from pages.models import Page

register = template.Library()

@register.inclusion_tag('pages/tags/put_page.html')
def pages_put_page(slug):
    try:
        page = Page.objects.all_published().get(slug=slug)
    except Page.DoesNotExist:
        page = None
    return {'page': page}

@register.inclusion_tag('pages/tags/put_body.html')
def pages_put_body(slug):
    try:
        page = Page.objects.all_published().get(slug=slug)
    except Page.DoesNotExist:
        page = None
    return {'page': page}
