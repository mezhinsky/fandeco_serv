from django.contrib.auth.signals import user_logged_in


def merge_cart(sender, user, request, **kwargs):
    cart = request.cart
    # lets overwrite the previous cart in case the user had a car
    if cart.is_empty():
        request.cart = cart.get_last_cart(user)
    else:
        cart.replace(cart.cart.id, user)


user_logged_in.connect(merge_cart)
