# -*- coding: utf-8 -*-
from django import template
from bshop.contrib.cart.models import Item, Cart
from bshop.contrib.cart.proxy import CartProxy

register = template.Library()

@register.inclusion_tag(
    'cart/ajax/cart.html',
    takes_context=True, name='show_cart'
)
def show_cart(context):
    request = context['request']
    cart = request.cart
    return {'cart':cart.cart,
             'request':request}
