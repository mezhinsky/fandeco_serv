# -*- coding: utf-8 -*-

from django.conf.urls import url, patterns
from . import views

urlpatterns = patterns("",
    url(r'^$', views.get_cart, name='cart_list',),
    url(r'^add/(?P<pk>\d+)/$', views.add_product_to_cart, name='add_product',),
    url(r'^remove/(?P<pk>\d+)/$', views.remove_product_to_cart, name='remove_product',),
    url(r'^update/(?P<pk>\d+)/$', views.update_to_cart, name='update_to_cart',),
    url(r'^clear/$', views.clear_cart, name='clear_cart',),
)
