# -*- coding: utf-8 -*-
from django.contrib import admin

from .models import Item, Cart


class ItemCartInline(admin.TabularInline):
    model = Item
    extra = 1


class CartAdmin(admin.ModelAdmin):
    inlines = [ItemCartInline]

admin.site.register(Cart, CartAdmin)


