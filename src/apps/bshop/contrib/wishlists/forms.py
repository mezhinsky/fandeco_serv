from django.forms import ModelForm

from .models import WishlistItem, Wishlist


class WishlistItemForm(ModelForm):
    """ Form for WishlistItem. """

    class Meta:
        model = WishlistItem
        fields = ['content_type', 'object_id']


class WishlistForm(ModelForm):
    """ Form for Wishlist. """

    class Meta:
        model = Wishlist
        fields = ['name']
    
    def __init__(self, *args, **kwargs):
        super(WishlistForm, self).__init__(*args, **kwargs)
        self.fields['name'].required = True
