from django import template
from django.core.cache import cache
from django.contrib.contenttypes.models import ContentType

from copy import copy
from bshop.contrib.wishlists.models import Wishlist, WishlistItem

register = template.Library()


def wishlist_form(context, item):
    """
    Render wishlist form for adding or removing a particular item.

    Assumes the user is available in the context and requires an instance of
    the wishlist model *not* WishlistItem.
    """

    # TODO: Replace by proper, meaningful, exceptions.
    assert 'user' in context
    assert hasattr(item, 'pk')
    assert hasattr(item, 'wishlistitem_set')

    # Copy local context dict, including
    context = copy(context)

    # Add item
    context['item'] = item

    # Return context for use in inclusion tag
    return context

# Register for adding
register.inclusion_tag(
    'wishlists/wishlistitem_add_form.html',
    takes_context=True, name='wishlist_add_form'
)(wishlist_form)

# Register for removing
register.inclusion_tag(
    'wishlists/wishlistitem_remove_form.html',
    takes_context=True, name='wishlist_remove_form'
)(wishlist_form)

@register.filter
def wishlists(user):
    cache_key = 'WISHLISTS_%s' % user.pk
    wishlist_set = cache.get(cache_key)
    if not wishlist_set:
        wishlist_set = [{'id':wishlist.pk, 'name':wishlist.name} for wishlist in user.wishlists.all()]
        cache.set(cache_key, wishlist_set)
    return wishlist_set

def user_wishlists(context):
    request = context['request']
    if request.user.is_authenticated():
        context['wishlist_count'] = WishlistItem.objects.filter(wishlist__user=request.user).count()
    else:
        context['wishlist_count'] = 0
    return context

register.inclusion_tag(
    'wishlists/tags/user_wishlists.html',
    takes_context=True, name='user_wishlists'
)(user_wishlists)

@register.filter
def wishlist_item_count(item):
    ctype_object = ContentType.objects.get_for_model(type(item),
                                              for_concrete_model=False)
    return WishlistItem.objects.filter(content_type=ctype_object, object_id=item.pk).count()
