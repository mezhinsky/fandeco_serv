# -*- coding: utf-8 -*-
from django.conf import settings

class Settings(object):
    def __init__(self, **kwargs):
        self.defaults = kwargs

    def __getattr__(self, key):
        return getattr(settings, 'WISHLISTS_%s' % key, self.defaults[key])


wishlist_settings = Settings(
    REDIRECT_URL = 'wishlist'
)