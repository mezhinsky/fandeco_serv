# -*- coding: utf-8 -*-

from django.utils import timezone
from treebeard.mp_tree import MP_NodeManager
from django.db import models


class CategoryManager(MP_NodeManager):

    def all_published(self):
        now = timezone.now()
        return super(CategoryManager, self).get_queryset().filter(
                    is_published=True, published_at__lte=now,)


class ProductManager(models.Manager):

    def all_published(self):
        now = timezone.now()
        return super(ProductManager, self).get_queryset().filter(
                    is_published=True, published_at__lte=now,)


class TagPageManager(models.Manager):

    def all_published(self):
        now = timezone.now()
        return super(TagPageManager, self).get_queryset().filter(
                    is_published=True, published_at__lte=now,)
