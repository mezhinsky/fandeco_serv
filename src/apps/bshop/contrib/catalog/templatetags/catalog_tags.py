# -*- coding: utf-8 -*-

from django import template
from bshop.contrib.catalog.models import Category, Manufacturer,\
    TagPage

register = template.Library()


@register.inclusion_tag('catalog/tags/tag_page_menu_list.html', takes_context=True)
def get_tag_page_menu(context):
    tag_page_menu_list = TagPage.objects.all_published()
    return {'tag_page_menu_list':tag_page_menu_list,
            'request':context['request'], }

@register.inclusion_tag('catalog/tags/categories.html', takes_context=True)
def categories(context):
    ctx = {'categories':Category.objects.all_published()}
    if 'category' in context:
        ctx['active'] = context['category'].pk
    return ctx

@register.inclusion_tag('catalog/tags/brands.html', takes_context=True)
def brands(context):
    ctx = {'brands':Manufacturer.objects.all()}
    if 'brand' in context:
        ctx['active'] = context['brand'].pk
    return ctx
