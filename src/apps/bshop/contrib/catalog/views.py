from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.shortcuts import get_object_or_404
from django.http import HttpResponsePermanentRedirect

from bshop.contrib.catalog.models import Product, Category, Manufacturer,\
    TagPage


class ProductListBase(ListView):
    template_name = "catalog/product_list.html"
    context_object_name="product_list"

    def get_queryset(self):
        return Product.objects.all_published()
        #.select_related('category').prefetch_related('brand')


class ProductDetailBase(DetailView):
    template_name = "catalog/product_detail.html"

    def get_queryset(self):
        return Product.objects.all_published()


class CategoryListView(ListView):
    template_name = "catalog/category_list.html"
    context_object_name="category_list"

    def get_queryset(self):
        return Category.objects.all_published()


class CategoryDetailView(ProductListBase):

    def get_queryset(self):
        self.category = get_object_or_404(Category.objects.all_published(),
                    pk=self.kwargs['pk'], slug=self.kwargs['slug'])
        qs = super(CategoryDetailView,self).get_queryset()
        return qs.filter(category=self.category)

    def get_context_data(self, **kwargs):
        ctx = super(CategoryDetailView, self).get_context_data(**kwargs)
        ctx['category'] = self.category
        return ctx


class TagPageDetailView(ProductListBase):

    def get_queryset(self):
        self.tag_page = get_object_or_404(TagPage.objects.all_published(),
                    pk=self.kwargs['pk'], slug=self.kwargs['slug'])
        qs = super(TagPageDetailView,self).get_queryset()
        return qs#.filter(category=self.category)

    def get_context_data(self, **kwargs):
        ctx = super(TagPageDetailView, self).get_context_data(**kwargs)
        ctx['tag_page'] = self.tag_page
        return ctx



class BrandListView(ProductListBase):

    def get_queryset(self):
        self.brand = get_object_or_404(Manufacturer,
                    pk=self.kwargs['pk'], slug=self.kwargs['slug'])
        qs = super(BrandListView,self).get_queryset()
        return qs.filter(brand=self.brand)

    def get_context_data(self, **kwargs):
        ctx = super(BrandListView,self).get_context_data(**kwargs)
        ctx['brand'] = self.brand
        return ctx


class ProductDetailView(ProductDetailBase):
    pass
