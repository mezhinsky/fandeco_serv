# -*- coding: utf-8 -*-

from django.conf.urls import url, patterns
from . import views

urlpatterns = patterns("",
        url(r'^catalog/$', views.CategoryListView.as_view(), name='catalog_index',),
        url(r'^(?P<slug>[-\w]+)/t(?P<pk>\d+)/$', views.TagPageDetailView.as_view(), name='catalog_tag_page_detail',),
        url(r'^(?P<slug>[-\w]+)/b(?P<pk>\d+)/$', views.BrandListView.as_view(), name='catalog_list_by_brand',),
        url(r'^(?P<slug>[-\w]+)/c(?P<pk>\d+)/$', views.CategoryDetailView.as_view(), name='catalog_list_by_category',),
        url(r'^(?P<slug>[-\w]+)/p(?P<pk>\d+)\.html$', views.ProductDetailView.as_view(), name='catalog_product_detail',),
#     url(r'^remove/(?P<pk>\d+)/$', views.remove_product_to_cart, name='remove_product',),
#     url(r'^update/(?P<pk>\d+)/$', views.update_to_cart, name='update_to_cart',),
)