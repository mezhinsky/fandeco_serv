# -*- coding: utf-8 -*-

from django.utils.translation import ugettext_lazy as _

import autocomplete_light
from .models import Manufacturer, AttributeValue


class ManufacturerAutocomplete(autocomplete_light.AutocompleteModelBase):
    search_fields = ['^title', '^slug']
    attrs = { # This will set the input placeholder attribute:
        'placeholder': _('title or slug'),
        'data-autocomplete-minimum-characters': 0, }

#     def choices_for_request(self):
#         if self.request.user.is_staff:
#             self.choices = self.choices.all()
#         else:
#             self.choices = self.choices.none()
#         return super(ManufacturerAutocomplete, self).choices_for_request()

autocomplete_light.register(Manufacturer, ManufacturerAutocomplete)


class AttributeValueAutocomplete(autocomplete_light.AutocompleteModelBase):
    search_fields = ['^attr__name', '^attr__code', ]
    attrs = { # This will set the input placeholder attribute:
        'placeholder': _('name or code'),
        'data-autocomplete-minimum-characters': 0, }

autocomplete_light.register(AttributeValue, AttributeValueAutocomplete)
