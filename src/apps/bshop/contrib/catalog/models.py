# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import os
from datetime import datetime, date

from django.db import models
from django.db.models.loading import get_model
from django.core.exceptions import ValidationError
from django.core.cache import cache
from django.core.urlresolvers import reverse
from django.core.validators import RegexValidator
from django.utils import six
from django.utils.encoding import python_2_unicode_compatible
from django.utils.html import strip_tags
from django.utils.safestring import mark_safe
from django.utils.translation import ugettext_lazy as _

from pytils.translit import slugify
from sorl.thumbnail.shortcuts import get_thumbnail
from treebeard.mp_tree import MP_Node

from bshop.core.models import TimeStampPubModel
from bshop.core.fields import NullCharField, CurrencyField
from bshop.contrib.catalog.managers import CategoryManager, TagPageManager,\
    ProductManager


@python_2_unicode_compatible
class Manufacturer(TimeStampPubModel):
    """
    Represents a Manufacturer
    """
    title = models.CharField(max_length=100, unique=True)
    slug = models.SlugField(max_length=100, unique=True)
    description = models.TextField(null=True, blank=True)

    class Meta:
        app_label = 'catalog'
        ordering = ('title',)
        verbose_name = _('manufacturer')
        verbose_name_plural = _('manufacturers')

    def __str__(self):
        return u"%s" % self.title

    def get_absolute_url(self):
        return reverse(
                'catalog_list_by_brand',
                kwargs={'slug': self.slug, 'pk': self.pk})


@python_2_unicode_compatible
class Category(MP_Node, TimeStampPubModel):
    """
    A product category. Merely used for navigational purposes; has no
    effects on business logic.

    Uses django-treebeard.
    """
    title = models.CharField(verbose_name=_('title'), max_length=254, db_index=True)
    slug = models.SlugField(verbose_name=_('slug'),max_length=254, db_index=True)
    body_html = models.TextField(_('description'), blank=True)
    description = models.TextField(verbose_name=_('short description'), blank=True)
    image = models.ImageField(_('image'), upload_to='bshop/category/%Y/%m/%d', blank=True, null=True,
            help_text=u"Для лучшего отображения на всех страницах сайта размер картинки предпочтителен 400х400 пикселей")
    count_products = models.PositiveIntegerField(verbose_name=_('products'), default=0)

    meta_title = models.CharField(verbose_name=_('meta title'), max_length=128, blank=True)
    meta_keywords = models.TextField(verbose_name=_('meta keywords'), max_length=600, blank=True)
    meta_description = models.TextField(verbose_name=_('meta description'), max_length=600, blank=True)

    node_order_by = ['title']
    _slug_separator = '/'
    _full_name_separator = ' > '

    objects = CategoryManager()

    class Meta:
        abstract = False
        app_label = 'catalog'
        ordering = ['path']
        verbose_name = _('category')
        verbose_name_plural = _('categories')

    def __str__(self):
        return u"%s" %  self.full_name

    def get_absolute_url(self):
        """
        Our URL scheme means we have to look up the category's ancestors. As
        that is a bit more expensive, we cache the generated URL. That is
        safe even for a stale cache, as the default implementation of
        ProductCategoryView does the lookup via primary key anyway. But if
        you change that logic, you'll have to reconsider the caching
        approach.
        """
        cache_key = 'BSHOP_CATEGORY_URL_%s' % self.pk
        url = cache.get(cache_key)
        if not url:
            url = reverse(
                'catalog_list_by_category',
                kwargs={'slug': self.slug, 'pk': self.pk})
            cache.set(cache_key, url)
        return url

    @property
    def full_name(self):
        """
        Returns a string representation of the category and it's ancestors,
        e.g. 'Books > Non-fiction > Essential programming'.

        It's rarely used in Oscar's codebase, but used to be stored as a
        CharField and is hence kept for backwards compatibility. It's also
        sufficiently useful to keep around.
        """
        names = [category.title for category in self.get_ancestors_and_self()]
        return self._full_name_separator.join(names)

    @property
    def full_slug(self):
        """
        Returns a string of this category's slug concatenated with the slugs
        of it's ancestors, e.g. 'books/non-fiction/essential-programming'.

        Oscar used to store this as in the 'slug' model field, but this field
        has been re-purposed to only store this category's slug and to not
        include it's ancestors' slugs.
        """
        slugs = [category.slug for category in self.get_ancestors_and_self()]
        return self._slug_separator.join(slugs)

    def generate_slug(self):
        """
        Generates a slug for a category. This makes no attempt at generating
        a unique slug.
        """
        return slugify(self.name)

    def ensure_slug_uniqueness(self):
        """
        Ensures that the category's slug is unique amongst it's siblings.
        This is inefficient and probably not thread-safe.
        """
        unique_slug = self.slug
        siblings = self.get_siblings().exclude(pk=self.pk)
        next_num = 2
        while siblings.filter(slug=unique_slug).exists():
            unique_slug = '{slug}_{end}'.format(slug=self.slug, end=next_num)
            next_num += 1

        if unique_slug != self.slug:
            self.slug = unique_slug
            self.save()

    def get_ancestors_and_self(self):
        """
        Gets ancestors and includes itself. Use treebeard's get_ancestors
        if you don't want to include the category itself. It's a separate
        function as it's commonly used in templates.
        """
        return list(self.get_ancestors()) + [self]

    def get_descendants_and_self(self):
        """
        Gets descendants and includes itself. Use treebeard's get_descendants
        if you don't want to include the category itself. It's a separate
        function as it's commonly used in templates.
        """
        return list(self.get_descendants()) + [self]

    def has_children(self):
        return self.get_num_children() > 0

    def get_num_children(self):
        return self.get_children().count()

    def save(self, *args, **kwargs):
        """
        Oscar traditionally auto-generated slugs from names. As that is
        often convenient, we still do so if a slug is not supplied through
        other means. If you want to control slug creation, just create
        instances with a slug already set, or expose a field on the
        appropriate forms.
        """
        if self.slug:
            # Slug was supplied. Hands off!
            super(Category, self).save(*args, **kwargs)
        else:
            self.slug = self.generate_slug()
            super(Category, self).save(*args, **kwargs)
            # We auto-generated a slug, so we need to make sure that it's
            # unique. As we need to be able to inspect the category's siblings
            # for that, we need to wait until the instance is saved. We
            # update the slug and save again if necessary.
            self.ensure_slug_uniqueness()


@python_2_unicode_compatible
class Product(TimeStampPubModel):
    title = models.CharField(verbose_name=_('title'), max_length=254, db_index=True)
    slug = models.SlugField(verbose_name=_('slug'),max_length=254, db_index=True)
    body_html = models.TextField(_('Full description'), null=True, blank=True,
                                help_text=_('Full description displayed on the product page'))
    description = models.TextField(verbose_name=_('short description'), blank=True)
    category = models.ForeignKey(Category, related_name='products', verbose_name=_('category'))
    brand = models.ForeignKey(Manufacturer, help_text='Manufacturer', blank=True, null=True)
    part_number = models.CharField(
        max_length=50, null=True, blank=True, help_text='Manufacturer part number')
    upc = NullCharField(_("UPC"), max_length=64, blank=True, null=True, unique=True,
        help_text=_("Universal Product Code (UPC) is an identifier for "
        "a product which is not specific to a particular "
        " supplier. Eg an ISBN for a book."))

    price = CurrencyField(verbose_name=_('Retail price'), blank=True, null=True)

    meta_title = models.CharField(verbose_name=_('meta title'), max_length=128, blank=True)
    meta_keywords = models.TextField(verbose_name=_('meta keywords'), max_length=600, blank=True)
    meta_description = models.TextField(verbose_name=_('meta description'), max_length=600, blank=True)

    objects = ProductManager()

    class Meta:
        app_label = 'catalog'
        verbose_name = _('product')
        verbose_name_plural = _('products')

    def __str__(self):
        return u"%s" % self.title

    def get_absolute_url(self):
        cache_key = 'BSHOP_PRODUCT_URL_%s' % self.pk
        url = cache.get(cache_key)
        if not url:
            url = reverse(
                'catalog_product_detail',
                kwargs={'slug': self.slug, 'pk': self.pk})
            cache.set(cache_key, url)
        return url

    def get_price(self):
        return self.price

    def get_price_out(self):
        return "%.0d<span> грн.</span>" % self.get_price()

    @property
    def image(self):
        try:
            img = self.images.all()[0]
        except IndexError:
            return None
        return img.file

    def get_admin_thumb(self):
        try:
            img = self.image
        except:
            img = False
        if img and getattr(img, "url", None):
            img_resize_url = unicode(get_thumbnail(img, '100x100').url)
        else:
            img_resize_url = "#"
        html = '<img src="%s" alt="%s"/>'
        return html % (img_resize_url, self.title)
    get_admin_thumb.short_description = u'Миниатюра'
    get_admin_thumb.allow_tags = True


@python_2_unicode_compatible
class ProductImage(models.Model):
    """
    Фото товара
    """
    product = models.ForeignKey(Product, verbose_name=_("Product"), related_name="images")
    file = models.ImageField(_("file"), upload_to="catalog/product/image/%Y/%m/%d", max_length=255,
            help_text=u"Для лучшего отображения на всех страницах сайта размер картинки предпочтителен 400х400 пикселей")
    is_main = models.BooleanField(_("is a main"), default=False)
    alt = models.CharField(_('alt'), max_length=255, blank=True)
    caption = models.CharField(_("Caption"), max_length=255, blank=True)
    # : Use display_order to determine which is the "primary" image
    display_order = models.PositiveIntegerField(
        _("Display Order"), default=0,
        help_text=_("An image with a display order of zero will be the primary"
                    " image for a product"))

    class Meta:
        app_label = 'catalog'
        verbose_name = _("image of product")
        verbose_name_plural = _("images of product")
        ordering = ('is_main', 'display_order','id')

    def __str__(self):
        return "%s" % self.file.name

    def filename(self):
        return os.path.basename(self.file.name)

    def get_admin_thumb(self):
        try:
            img = self.file
        except:
            img = False
        if img and getattr(img, "url", None):
            img_resize_url = unicode(get_thumbnail(img, '100x100').url)
        else:
            img_resize_url = "#"
        html = '<img src="%s" alt="%s"/>'
        return html % (img_resize_url, self.alt)
    get_admin_thumb.short_description = u'Миниатюра'
    get_admin_thumb.allow_tags = True


@python_2_unicode_compatible
class Attribute(models.Model):
    """
    Defines an attribute for a product class. (For example, number_of_pages for
    a 'book' class)
    """
    name = models.CharField(verbose_name=_('name'), max_length=128, db_index=True)
    code = models.SlugField(
        _('Code'), max_length=128,
        validators=[RegexValidator(
            regex=r'^[a-zA-Z\-_][0-9a-zA-Z\-_]*$',
            message=_("Code can only contain the letters a-z, A-Z, digits, "
                      "minus and underscores, and can't start with a digit"))])
    # Attribute types
    CHAR = "char"
    TEXT = "text"
    INTEGER = "integer"
    BOOLEAN = "boolean"
    FLOAT = "float"
    RICHTEXT = "richtext"
    DATE = "date"
    TYPE_CHOICES = (
        (CHAR, _("Char (128 chars)")),
        (TEXT, _("Text")),
        (INTEGER, _("Integer")),
        (BOOLEAN, _("True / False")),
        (FLOAT, _("Float")),
        (RICHTEXT, _("Rich Text")),
        (DATE, _("Date")),
    )
    type = models.CharField(
        choices=TYPE_CHOICES, default=TYPE_CHOICES[0][0],
        max_length=20, verbose_name=_("Type"))

    class Meta:
        app_label = 'catalog'
        verbose_name = _("attribute")
        verbose_name_plural = _("attributes")

    def __str__(self):
        return self.name

    def validate_value(self, value):
        validator = getattr(self, '_validate_%s' % self.type)
        validator(value)

    # Validators

    def _validate_text(self, value):
        if not isinstance(value, six.string_types):
            raise ValidationError(_("Must be str or unicode"))
    _validate_richtext = _validate_text
    _validate_char = _validate_text

    def _validate_float(self, value):
        try:
            float(value)
        except ValueError:
            raise ValidationError(_("Must be a float"))

    def _validate_integer(self, value):
        try:
            int(value)
        except ValueError:
            raise ValidationError(_("Must be an integer"))

    def _validate_date(self, value):
        if not (isinstance(value, datetime) or isinstance(value, date)):
            raise ValidationError(_("Must be a date or datetime"))

    def _validate_boolean(self, value):
        if not type(value) == bool:
            raise ValidationError(_("Must be a boolean"))

    def save_value(self, product, value):
        AttributeValue = get_model('catalog', 'AttributeValue')
        try:
            value_obj = product.a_values.get(attr=self)
        except AttributeValue.DoesNotExist:
            # FileField uses False for announcing deletion of the file
            # not creating a new value
            if value is None or value == '':
                return
            value_obj = AttributeValue.objects.create(
                product=product, attr=self)

            if value is None or value == '':
                value_obj.delete()
                return
            if value != value_obj.value:
                value_obj.value = value
                value_obj.save()


@python_2_unicode_compatible
class AttributeValue(models.Model):
    """
    The "through" model for the m2m relationship between catalog.Product and
    catalog.Attribute. This specifies the value of the attribute for
    a particular product
    For example: number_of_pages = 295s
    """
    attr = models.ForeignKey(Attribute, verbose_name=_("Attribute"), related_name="a_values")
    product = models.ForeignKey(Product, verbose_name=_("Product"), related_name="a_values")
    value_char = models.CharField(_(u'Char'), max_length=128, blank=True, null=True)
    value_text = models.TextField(_('Text'), blank=True, null=True)
    value_richtext = models.TextField(_('Richtext'), blank=True, null=True)
    value_integer = models.IntegerField(_('Integer'), blank=True, null=True)
    value_boolean = models.NullBooleanField(_('Boolean'), blank=True)
    value_float = models.FloatField(_('Float'), blank=True, null=True)
    value_date = models.DateField(_('Date'), blank=True, null=True)
    display_order = models.IntegerField(default=0)

    class Meta:
        app_label = 'catalog'
        unique_together = (('product', 'attr'),)
        verbose_name = _("attribute value")
        verbose_name_plural = _("attribute values")

    def __str__(self):
        return self.summary()

    def _get_value(self):
        return getattr(self, 'value_%s' % self.attr.type)

    def _set_value(self, new_value):
        setattr(self, 'value_%s' % self.attr.type, new_value)

    value = property(_get_value, _set_value)


    def summary(self):
        """
        Gets a string representation of both the attribute and it's value,
        used e.g in product summaries.
        """
        return u"%s: %s" % (self.attr.name, self.value_as_text)

    @property
    def value_as_text(self):
        """
        Returns a string representation of the attribute's value. To customise
        e.g. image attribute values, declare a _image_as_text property and
        return something appropriate.
        """
        property_name = '_%s_as_text' % self.attr.type
        return getattr(self, property_name, self.value)

    @property
    def _richtext_as_text(self):
        return strip_tags(self.value)

    @property
    def value_as_html(self):
        """
        Returns a HTML representation of the attribute's value. To customise
        e.g. image attribute values, declare a _image_as_html property and
        return e.g. an <img> tag.  Defaults to the _as_text representation.
        """
        property_name = '_%s_as_html' % self.attr.type
        return getattr(self, property_name, self.value_as_text)

    @property
    def _richtext_as_html(self):
        return mark_safe(self.value)


@python_2_unicode_compatible
class TagPage(TimeStampPubModel):
    """
    Tag - страницы
    """
    title = models.CharField(_(u'Наименование'), max_length=240,
            help_text=u"Наименование, отображаемое при просмотре на сайте")
    slug = models.SlugField(max_length=240, unique=True)
    body_html = models.TextField(_(u'Описание'), blank=True, null=True)
    image = models.ImageField(_('image'), blank=True, null=True,
                              upload_to="bshop/tag-page/cover/%Y/%m/%d",)
    categories = models.ManyToManyField(Category, blank=True,
            verbose_name=_(u'Categories'), related_name="tagpages",
            help_text=u"Разрешается указать Category которые будут подобраны для этой страницы услуги")

    display_order = models.IntegerField(default=0)

    objects = TagPageManager()

    class Meta:
        app_label = 'catalog'
        ordering = ('display_order','id')
        verbose_name = _("tagged page")
        verbose_name_plural = _("tagged pages")

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        cache_key = 'BSHOP_TAG_PAGE_URL_%s' % self.pk
        url = cache.get(cache_key)
        if not url:
            url = reverse(
                'catalog_tag_page_detail',
                kwargs={'slug': self.slug, 'pk': self.pk})
            cache.set(cache_key, url)
        return url

