# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.utils.timezone
import apps.bshop.core.fields
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Attribute',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=128, verbose_name='name', db_index=True)),
                ('code', models.SlugField(max_length=128, verbose_name='Code', validators=[django.core.validators.RegexValidator(regex=b'^[a-zA-Z\\-_][0-9a-zA-Z\\-_]*$', message="Code can only contain the letters a-z, A-Z, digits, minus and underscores, and can't start with a digit")])),
                ('type', models.CharField(default=b'char', max_length=20, verbose_name='Type', choices=[(b'char', 'Char (128 chars)'), (b'text', 'Text'), (b'integer', 'Integer'), (b'boolean', 'True / False'), (b'float', 'Float'), (b'richtext', 'Rich Text'), (b'date', 'Date')])),
            ],
            options={
                'verbose_name': 'Attribute',
                'verbose_name_plural': 'Attributes',
            },
        ),
        migrations.CreateModel(
            name='AttributeValue',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('value_char', models.CharField(max_length=128, null=True, verbose_name='Char', blank=True)),
                ('value_text', models.TextField(null=True, verbose_name='Text', blank=True)),
                ('value_richtext', models.TextField(null=True, verbose_name='Richtext', blank=True)),
                ('value_integer', models.IntegerField(null=True, verbose_name='Integer', blank=True)),
                ('value_boolean', models.NullBooleanField(verbose_name='Boolean')),
                ('value_float', models.FloatField(null=True, verbose_name='Float', blank=True)),
                ('value_date', models.DateField(null=True, verbose_name='Date', blank=True)),
                ('display_order', models.IntegerField(default=0)),
                ('attr', models.ForeignKey(related_name='a_values', verbose_name='Attribute', to='catalog.Attribute')),
            ],
            options={
                'verbose_name': 'Attribute value',
                'verbose_name_plural': 'Attribute values',
            },
        ),
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('path', models.CharField(unique=True, max_length=255)),
                ('depth', models.PositiveIntegerField()),
                ('numchild', models.PositiveIntegerField(default=0)),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='created at')),
                ('updated_at', models.DateTimeField(auto_now=True, verbose_name='updated at')),
                ('is_published', models.BooleanField(default=True, verbose_name='published')),
                ('published_at', models.DateTimeField(default=django.utils.timezone.now, verbose_name='published at')),
                ('title', models.CharField(max_length=254, verbose_name='title', db_index=True)),
                ('slug', models.SlugField(max_length=254, verbose_name='slug')),
                ('body_html', models.TextField(verbose_name='description', blank=True)),
                ('description', models.TextField(verbose_name='short description', blank=True)),
                ('image', models.ImageField(help_text='\u0414\u043b\u044f \u043b\u0443\u0447\u0448\u0435\u0433\u043e \u043e\u0442\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u044f \u043d\u0430 \u0432\u0441\u0435\u0445 \u0441\u0442\u0440\u0430\u043d\u0438\u0446\u0430\u0445 \u0441\u0430\u0439\u0442\u0430 \u0440\u0430\u0437\u043c\u0435\u0440 \u043a\u0430\u0440\u0442\u0438\u043d\u043a\u0438 \u043f\u0440\u0435\u0434\u043f\u043e\u0447\u0442\u0438\u0442\u0435\u043b\u0435\u043d 400\u0445400 \u043f\u0438\u043a\u0441\u0435\u043b\u0435\u0439', upload_to=b'bshop/category/%Y/%m/%d', null=True, verbose_name='image', blank=True)),
                ('count_products', models.PositiveIntegerField(default=0, verbose_name='products')),
                ('meta_title', models.CharField(max_length=128, verbose_name='meta title', blank=True)),
                ('meta_keywords', models.TextField(max_length=600, verbose_name='meta keywords', blank=True)),
                ('meta_description', models.TextField(max_length=600, verbose_name='meta description', blank=True)),
            ],
            options={
                'ordering': ['path'],
                'verbose_name': 'Category',
                'verbose_name_plural': 'Categories',
            },
        ),
        migrations.CreateModel(
            name='Manufacturer',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='created at')),
                ('updated_at', models.DateTimeField(auto_now=True, verbose_name='updated at')),
                ('is_published', models.BooleanField(default=True, verbose_name='published')),
                ('published_at', models.DateTimeField(default=django.utils.timezone.now, verbose_name='published at')),
                ('title', models.CharField(unique=True, max_length=100)),
                ('slug', models.SlugField(unique=True, max_length=100)),
                ('description', models.TextField(null=True, blank=True)),
            ],
            options={
                'ordering': ('title',),
            },
        ),
        migrations.CreateModel(
            name='Product',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='created at')),
                ('updated_at', models.DateTimeField(auto_now=True, verbose_name='updated at')),
                ('is_published', models.BooleanField(default=True, verbose_name='published')),
                ('published_at', models.DateTimeField(default=django.utils.timezone.now, verbose_name='published at')),
                ('title', models.CharField(max_length=254, verbose_name='title', db_index=True)),
                ('slug', models.SlugField(max_length=254, verbose_name='slug')),
                ('body_html', models.TextField(help_text='Full description displayed on the product page', null=True, verbose_name='Full description', blank=True)),
                ('description', models.TextField(verbose_name='short description', blank=True)),
                ('part_number', models.CharField(help_text=b'Manufacturer part number', max_length=50, null=True, blank=True)),
                ('upc', apps.bshop.core.fields.NullCharField(max_length=64, help_text='Universal Product Code (UPC) is an identifier for a product which is not specific to a particular  supplier. Eg an ISBN for a book.', unique=True, verbose_name='UPC')),
                ('price', apps.bshop.core.fields.CurrencyField(null=True, verbose_name='Retail price', max_digits=15, decimal_places=2, blank=True)),
                ('meta_title', models.CharField(max_length=128, verbose_name='meta title', blank=True)),
                ('meta_keywords', models.TextField(max_length=600, verbose_name='meta keywords', blank=True)),
                ('meta_description', models.TextField(max_length=600, verbose_name='meta description', blank=True)),
                ('brand', models.ForeignKey(blank=True, to='catalog.Manufacturer', help_text=b'Manufacturer', null=True)),
                ('category', models.ForeignKey(related_name='products', verbose_name='category', to='catalog.Category')),
            ],
            options={
                'verbose_name': 'Product',
                'verbose_name_plural': 'Products',
            },
        ),
        migrations.CreateModel(
            name='ProductImage',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('file', models.ImageField(help_text='\u0414\u043b\u044f \u043b\u0443\u0447\u0448\u0435\u0433\u043e \u043e\u0442\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u044f \u043d\u0430 \u0432\u0441\u0435\u0445 \u0441\u0442\u0440\u0430\u043d\u0438\u0446\u0430\u0445 \u0441\u0430\u0439\u0442\u0430 \u0440\u0430\u0437\u043c\u0435\u0440 \u043a\u0430\u0440\u0442\u0438\u043d\u043a\u0438 \u043f\u0440\u0435\u0434\u043f\u043e\u0447\u0442\u0438\u0442\u0435\u043b\u0435\u043d 400\u0445400 \u043f\u0438\u043a\u0441\u0435\u043b\u0435\u0439', upload_to=b'catalog/product/image/%Y/%m/%d', max_length=255, verbose_name='file')),
                ('is_main', models.BooleanField(default=False, verbose_name='is a main')),
                ('alt', models.CharField(max_length=255, verbose_name='alt', blank=True)),
                ('caption', models.CharField(max_length=255, verbose_name='Caption', blank=True)),
                ('display_order', models.PositiveIntegerField(default=0, help_text='An image with a display order of zero will be the primary image for a product', verbose_name='Display Order')),
                ('product', models.ForeignKey(related_name='images', verbose_name='Product', to='catalog.Product')),
            ],
            options={
                'ordering': ('is_main', 'display_order'),
                'verbose_name': 'Image of product',
                'verbose_name_plural': 'images or product',
            },
        ),
        migrations.AddField(
            model_name='attributevalue',
            name='product',
            field=models.ForeignKey(related_name='a_values', verbose_name='Product', to='catalog.Product'),
        ),
        migrations.AlterUniqueTogether(
            name='attributevalue',
            unique_together=set([('product', 'attr')]),
        ),
    ]
