# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0002_auto_20150420_1458'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='attribute',
            options={'verbose_name': 'attribute', 'verbose_name_plural': 'attributes'},
        ),
        migrations.AlterModelOptions(
            name='attributevalue',
            options={'verbose_name': 'attribute value', 'verbose_name_plural': 'attribute values'},
        ),
        migrations.AlterModelOptions(
            name='category',
            options={'ordering': ['path'], 'verbose_name': 'category', 'verbose_name_plural': 'categories'},
        ),
        migrations.AlterModelOptions(
            name='manufacturer',
            options={'ordering': ('title',), 'verbose_name': 'manufacturer', 'verbose_name_plural': 'manufacturers'},
        ),
        migrations.AlterModelOptions(
            name='product',
            options={'verbose_name': 'product', 'verbose_name_plural': 'products'},
        ),
        migrations.AlterModelOptions(
            name='productimage',
            options={'ordering': ('is_main', 'display_order', 'id'), 'verbose_name': 'image of product', 'verbose_name_plural': 'images of product'},
        ),
    ]
