# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import bshop.core.fields


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='product',
            name='price',
            field=bshop.core.fields.CurrencyField(null=True, verbose_name='Retail price', max_digits=15, decimal_places=2, blank=True),
        ),
        migrations.AlterField(
            model_name='product',
            name='upc',
            field=bshop.core.fields.NullCharField(max_length=64, help_text='Universal Product Code (UPC) is an identifier for a product which is not specific to a particular  supplier. Eg an ISBN for a book.', unique=True, verbose_name='UPC'),
        ),
    ]
