# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.utils import timezone


class TimeStampModel(models.Model):
    """
    An abstract base class model that provides self-updating
    ``created_at`` and ``updated_at`` fields.

    """
    created_at = models.DateTimeField(_("created at"), auto_now_add=True)
    updated_at = models.DateTimeField(_('updated at'), auto_now=True)

    class Meta:
        abstract = True


class TimeStampPubModel(TimeStampModel):
    """
    An abstract base class model that provides self-updating
    ``created_at`` and ``updated_at``
    ``is_published`` and ``published_at`` fields.

    """
    is_published = models.BooleanField(default=True, verbose_name=_('published'))
    published_at = models.DateTimeField(_("published at"), default=timezone.now)

    class Meta:
        abstract = True


class MetaDataModel(models.Model):

    meta_title = models.CharField(verbose_name=_('meta title'), max_length=128, blank=True)
    meta_keywords = models.TextField(verbose_name=_('meta keywords'), max_length=600, blank=True)
    meta_description = models.TextField(verbose_name=_('meta description'), max_length=600, blank=True)

    class Meta:
        abstract = True