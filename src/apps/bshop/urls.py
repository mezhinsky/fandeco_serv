# -*- coding: utf-8 -*-
from django.conf import settings
from django.conf.urls import patterns, url, include


urlpatterns = patterns('',
    url(r'^wishlists/', include('bshop.contrib.wishlists.urls')),
    url(r'^cart/', include('bshop.contrib.cart.urls')),
    url(r'^', include('bshop.contrib.catalog.urls')),
)

# if 'bshop' in settings.INSTALLED_APPS:
#     urlpatterns += patterns('',
#         (r'^activate/', include('userprofiles.contrib.accountverification.urls')),
#     )



