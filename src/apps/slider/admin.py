from django.contrib import admin
from slider.models import HSlider


@admin.register(HSlider)
class HSliderAdmin(admin.ModelAdmin):
    model = HSlider
    list_display = ('id', 'name', 'is_published', 'image')
    list_display_links = ('name',)
    list_filter = ('is_published',)
    list_editable = ('is_published',)
    list_per_page = 25
    list_max_show_all = 50

