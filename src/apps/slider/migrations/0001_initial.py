# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='HSlider',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255, verbose_name='name')),
                ('caption', models.CharField(max_length=255, verbose_name='text', blank=True)),
                ('image', models.ImageField(upload_to=b'slider/images/%Y/%m/%d', verbose_name='image')),
                ('url', models.CharField(max_length=255, verbose_name='url', blank=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='created at')),
                ('rate', models.IntegerField(default=0, help_text='The larger the value, the slider above', verbose_name='sorting')),
                ('is_published', models.BooleanField(default=True, verbose_name='published')),
            ],
            options={
                'ordering': ['created_at'],
                'verbose_name': 'Slider',
                'verbose_name_plural': 'Sliders',
            },
        ),
    ]
