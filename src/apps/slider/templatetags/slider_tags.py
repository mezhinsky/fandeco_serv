# -*- coding: utf-8 -*-

from django import template
from slider.models import HSlider

register = template.Library()


@register.inclusion_tag('slider/tags/sliding_on_home.html')
def get_slide_on_home(number):
    return {'slide_list': HSlider.objects.filter(is_published=True).order_by('-rate', '-created_at')[:number]}

