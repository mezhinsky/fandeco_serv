from django.contrib import admin

from .models import Company


class CompanyAdmin(admin.ModelAdmin):
    pass

MODELS = {
    Company: CompanyAdmin,  
}

for model_or_iterable, admin_class in MODELS.items():
    admin.site.register(model_or_iterable, admin_class)