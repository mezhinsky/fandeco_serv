# coding: utf-8
from django.db import models

class Company(models.Model):
  name = models.CharField(u'Имя компании', max_length=100)
  domain = models.CharField(u'Домен', max_length=100)

  class Meta:
    verbose_name = u"Компания"
    verbose_name_plural = u"Компании"

  def __str__(self):
      return self.name

  def __unicode__(self):
      return self.name