# coding: utf-8

from django.conf import settings

import urlparse

from .models import Company

class SubdomainMiddleware(object):

    def process_request(self, request):
        request.no_company = False
        split = urlparse.urlparse(request.build_absolute_uri()).hostname.split('.')
        request.slug = split[0]
        the_rest =  '.'.join(split[1:])
        if settings.BASE_DOMAIN == the_rest:
            if request.slug in settings.MAIN_SITE:
                request.slug = None
                request.main_site = True
                request.current_company = None
                request.no_company = True
                return None
            #Company is using a subdomain.
            try:
                request.main_site = False
                current_company = Company.objects.get(domain = request.slug)
                request.current_company = current_company
            except Company.DoesNotExist:
                request.no_company = True
                # OH SHIT!
            return None