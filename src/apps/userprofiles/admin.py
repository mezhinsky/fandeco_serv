# -*- coding: utf-8 -*-
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

from userprofiles.settings import up_settings
from userprofiles.contrib.profiles.models import User
from userprofiles.contrib.accountverification.models import AccountVerification
from userprofiles.contrib.accountverification.actions import resend_activation_email


if up_settings.USE_PROFILE and up_settings.INLINE_PROFILE_ADMIN:

    admin.site.unregister(User)

    class UserProfileInline(admin.StackedInline):
        model = User
        extra = 1
        max_num = 1

    class AccountVerificationInline(admin.StackedInline):
        model = AccountVerification
        extra = 0
        # max_num = 1

    class UserProfileAdmin(UserAdmin):
        inlines = [AccountVerificationInline, UserProfileInline]
        list_display = 'username', 'email', 'first_name', 'last_name', 'is_active', 'is_staff'
        actions = [resend_activation_email, ]

    admin.site.register(User, UserProfileAdmin)


@admin.register(User)
class UserProfileAdmins(admin.ModelAdmin):
    list_display = ('id', 'get_email')
    list_display_links = ('id',)
    search_fields = ('=id', 'username', 'first_name', 'last_name', 'email', )

    def get_email(self, obj):
        return obj.user.email
    get_email.short_description = u'email'


