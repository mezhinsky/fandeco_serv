��    [      �     �      �     �     �  +   �     )  )   6     `     o     �     �     �  	   �     �     �     �     �  *   �     	     *	     7	     G	     V	     [	     j	     q	  #   �	     �	     �	     �	     �	     �	  
   �	     
     
     '
     D
  	   b
  
   l
     w
     }
  	   �
  
   �
  
   �
     �
  &   �
     �
     �
               -  c   3  e   �     �               )     ?     N     R  �   Z  8   �  E     %   ]  N   �     �  B   �     -  >   3     r     w     �     �     �  @   �  t   �  y   W  &   �  '   �        *   6  !   a  �   �     q     u  	   �  
   �     �     �     �     �  *   �  �     !   �  (   �  Z   #     ~  P   �     �  .   �  !   "  !   D     f  !   �     �  
   �     �     �  ^   �  =   [  '   �     �     �     �                %  /   ;  !   k     �     �  7   �  
   �                  #   -  J   Q     �  
   �  
   �  !   �     �     �          !  E   >  T   �     �     �  !        '  �   6  �   	  5   �     �     �  )        <     Z     a    n  h   �  �   �  2   p  �   �  :   8  �   s     �  �         �   5   �   '   �      	!     )!  �   :!  �   �!  �   �"  2   n#  A   �#  &   �#  L   
$  <   W$  t  �$     	&  !   &      .&     O&     _&  @   n&     �&  !   �&  x   �&     Y   6      ;          ,      3   '   8   L              2              7   .   &           !          Z   0                            M                     H          	   B       V          =      -   W                      "   E      U      #              O       
   P         Q   >               (   %   R             5           S          4   *   ?   @   D   N   X         <   J      $   9   /       A   [   +   G   C   )      K      F                   T   :   I   1    %(count)d records were %(email)s already activated. %(msg)s successfully send activation email. 1 record was A user with that username already exists. About yourself Account activation Account verification Account verifications Activation key Add image Add shop Address Approved Birthday By clicking 'Create account' I agree that: Change e-mail address Change image Change password Change profile Code Create account E-mail E-mail (repeat) E-mail address changed to %(email)s E-mail confirmation E-mail verification E-mail verifications Expiration date Expired First name Go to I am at least 18 years old. I have read and accepted the In order to vote, you should  Last name Logged out Login Mobile phone My orders My profile My reviews New e-mail address No Password? Reset your password here. No account? Register here. Old e-mail address Password Password (repeat) Phone Please click on the following link to change your email address:

http://%(domain)s%(url)s

Cheers! Please click on the following link to reset your password:

%(protocol)s://%(domain)s%(url)s

Cheers! Privacy Policy Profile changed Registration Registration complete Reset password Sex Sign in Thank you for creating an account.

Please click on the following link to activate your account:

http://%(domain)s%(url)s

Cheers! The reset link is invalid. Please double check the link. The submitted activation code is invalid. Activation is not possible. The two email addresses do not match. This email address is already in use. Please supply a different email address. This field is required. This value may contain only letters, numbers and ./-/_ characters. Token Unable to change e-mail address. Confirmation link is invalid. User User Agreement User profile Username View We activated your account. You are now able to log in. Have fun! We send you a e-mail including a link. Please click the
        link to continue resetting your password. Thank you! We send you a e-mail including a link. Please click the link to
        continue changing your e-mail address. Thank you! We successfully changed your password. We successfully resetted your password. You are not logged in You must type the same password each time. Your registration was successful. Your registration was successful. We send you a e-mail including a link.<br />
	            Please click the link to activate your account. Thank you!<br />
	            <br />
	            The link is valid for %(expiration_days)s days. and authorization form authorize created at home opens in a new window or tab. or registration form resend activation email for selected users Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2014-07-01 23:47+0300
PO-Revision-Date: 2014-06-04 13:39+0300
Last-Translator:    <admin@admin.com>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Translated-Using: django-rosetta 0.7.4
 %(count)d записей были %(email)s уже активирован. %(msg)s успешно отправлено письмо с кодом активации. 1 записи было Пользователь с таким именем уже существует. О себе Активация учетной записи Проверка аккаунта Проверки аккаунта Ключ активации Добавить картинку Добавить магазин Адрес Утвержден День рождения Нажав кнопку 'Создать аккаунт' я согласен с тем, что: Изменить адрес электронной почты Изменить изображение Смена пароля Изменить профиль Код Создать аккаунт E-mail E-mail (повтор) E-mail адрес изменен на %(email)s Подтверждение E-mail Проверка E-mail Проверки E-mail Дата истечения срока действия Истек Имя Перейти на Мне 16 лет. Я прочитал и принял Для того, чтобы проголосовать, вы должны  Фамилия Выйти Логин Мобильный телефон Мои заказы Мой профиль Мои отзывы Новый e-mail адрес Без Пароля? Сбросить Ваш пароль здесь. Нет учетной записи? Зарегистрироваться здесь. Старый e-mail адрес Пароль Пароль (повторить) Телефон Пожалуйста, нажмите на следующую ссылку, чтобы изменить свой адрес электронной почты:

http://%(domain)s%(url)s

Будьте с нами! Пожалуйста, нажмите на следующую ссылку для восстановления пароля:

%(protocol)s://%(domain)s%(url)s Политики Конфиденциальности Профиль изменен Регистрация Регистрация завершена Сбросить пароль Пол Ввойти Спасибо за создание учетной записи.

Пожалуйста, нажмите на следующую ссылку для активации Вашей учетной записи:

http://%(domain)s%(url)s

Вперед! Будьте с нами! Ссылка сброса недопустима. Пожалуйста, проверьте ссылку. Отправленный код активации является недопустимым. Активация невозможна. Два email-адреса не совпадают. Этот адрес уже используется. Пожалуйста, укажите другой адрес электронной почты. Это поле является обязательным. Это значение может содержать только латинские буквы, цифры и ./-/_ символы. Ключ Не удается изменить свой e-mail адрес. Ссылка для подтверждения недействительна. Пользователь Пользовательское Соглашение Профиль пользователя Имя пользователя Просмотр Мы активировали Ваш аккаунт. Теперь вы можете войти в систему. Добро пожаловать! Мы отправим вам на e-mail  ссылку. Пожалуйста, нажмите на
ссылку для продолжения сброса вашего пароля. Спасибо! Мы отправим вам e-mail, включая ссылку. Пожалуйста, нажмите на ссылку
для продолжения изменения вашего e-mail адреса. Спасибо! Мы успешно изменили пароль. Мы успешно восстановили Ваш пароль. Вы не Авторизированы Вы должны ввести тот же пароль каждый раз. Ваша регистрация прошла успешно. Ваша регистрация прошла успешно. Мы отправили Вам на e-mail ссылку подтверждения .<br /> Пожалуйста, нажмите на ссылку для активации Вашей учетной записи. Спасибо! <br /> <br/> Ссылка действительна  %(expiration_days)s дней. и форму авторизации авторизироваться создан в главную откроется в новом окне или вкладке. или форма регистрации Повторно отправить письмо для активации выбранных пользователей 