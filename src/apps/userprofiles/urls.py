# -*- coding: utf-8 -*-
from django.conf import settings
from django.conf.urls import patterns, url, include


urlpatterns = patterns('userprofiles.views',
    url(r'^register/$', 'registration', name='userprofiles_registration'),
    url(r'^register/complete/$', 'registration_complete',
        name='userprofiles_registration_complete'),
)

if 'userprofiles.contrib.accountverification' in settings.INSTALLED_APPS:
    urlpatterns += patterns('',
        (r'^activate/', include('userprofiles.contrib.accountverification.urls')),
    )

if 'userprofiles.contrib.emailverification' in settings.INSTALLED_APPS:
    urlpatterns += patterns('',
        (r'^email/', include('userprofiles.contrib.emailverification.urls')),
    )

if 'userprofiles.contrib.profiles' in settings.INSTALLED_APPS:
    urlpatterns += patterns('',
        (r'^profile/', include('userprofiles.contrib.profiles.urls')),
    )

urlpatterns += patterns('django.contrib.auth.views',
    url(r'^login/$', 'login', {'template_name': 'userprofiles/login.html'}, name='login'),
    url(r'^logout/$', 'logout', {'template_name': 'userprofiles/logged_out.html'}, name='logout'),
    url(r'^password_change/$', 'password_change',
            {'template_name': 'userprofiles/password_change.html'}, name='password_change'),
    url(r'^password_change/done/$', 'password_change_done',
            {'template_name': 'userprofiles/password_change_done.html'}, name='password_change_done'),
    url(r'^password_reset/$', 'password_reset', {'template_name': 'userprofiles/password_reset.html',
         'email_template_name': 'userprofiles/mails/password_reset_email.html'},
         name='password_reset'),
    url(r'^password_reset/done/$', 'password_reset_done',
            {'template_name': 'userprofiles/password_reset_done.html'}, name='password_reset_done'),
    # Support old style base36 password reset links; remove in Django 1.7
#    url(r'^reset/(?P<uidb36>[0-9A-Za-z]{1,13})-(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
#        'password_reset_confirm_uidb36', {'template_name': 'userprofiles/password_reset_confirm.html'},),
    url(r'^reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        'password_reset_confirm', {'template_name': 'userprofiles/password_reset_confirm.html'},
        name='password_reset_confirm'),
    url(r'^reset/done/$', 'password_reset_complete',
            {'template_name': 'userprofiles/password_reset_complete.html'},
        name='password_reset_complete'),
)


