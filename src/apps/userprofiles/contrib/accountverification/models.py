# -*- coding: utf-8 -*-
from datetime import timedelta
import hashlib
import random
import re
import uuid

from django.conf import settings
from django.contrib.sites.models import Site
from django.core.mail import send_mail
from django.db import models
from django.template.loader import render_to_string
from django.utils.translation import ugettext_lazy as _
from django.utils import timezone

from userprofiles.settings import up_settings
from django.contrib.auth import get_user_model
from django.utils.encoding import python_2_unicode_compatible

SHA1_RE = re.compile('^[a-f0-9]{40}$')


class AccountVerificationManager(models.Manager):
    def activate_user(self, activation_key):
        if SHA1_RE.search(activation_key):
            try:
                verification = self.get(activation_key=activation_key)
            except self.model.DoesNotExist:
                return False

            if not verification.activation_key_expired():
                user = verification.user
                user.is_active = True
                user.save()

                verification.activation_key = self.model.ACTIVATED
                verification.save()

                return user

        return False

    def create_inactive_user(self, password, email):
#        new_user = get_user_model().objects.create_user(username, email, password)
        new_user = get_user_model().objects.create_user(email, password)
        new_user.is_active = False
        new_user.save()

        account_verification = self.create_verification(new_user)
        account_verification.send_templated_email()

        """
        subject = ''.join(render_to_string(
            'userprofiles/mails/activation_email_subject.html',
            {'site': current_site}).splitlines())

        message = render_to_string('userprofiles/mails/activation_email.html', {
            'activation_key': account_verification.activation_key,
            'expiration_days': up_settings.ACCOUNT_VERIFICATION_DAYS,
            'site': current_site})

        send_mail(subject, message, settings.DEFAULT_FROM_EMAIL, [new_user.email])
        """
        return new_user

    def create_verification(self, user):
        salt = hashlib.sha1(str(random.random())).hexdigest()[:5]
        activation_key = hashlib.sha1(salt + str(uuid.uuid4())).hexdigest()
        return self.create(user=user, activation_key=activation_key)

    def delete_expired_users(self):
        for verification in self.all():
            if verification.activation_key_expired():
                user = verification.user
                if not user.is_active:
                    user.delete()


@python_2_unicode_compatible
class AccountVerification(models.Model):
    ACTIVATED = 'ALREADY_ACTIVATED'

    user = models.ForeignKey(settings.AUTH_USER_MODEL, verbose_name=_('User'))
    activation_key = models.CharField(_('Activation key'), max_length=40)

    objects = AccountVerificationManager()

    class Meta:
        verbose_name = _('Account verification')
        verbose_name_plural = _('Account verifications')

    def __str__(self):
        return u'Account verification: %s' % self.user

    def activation_key_expired(self):
        expiration_date = timedelta(days=up_settings.ACCOUNT_VERIFICATION_DAYS)
        return (self.activation_key == self.ACTIVATED
            or (self.user.date_joined + expiration_date <= timezone.now()))
    activation_key_expired.boolean = True

    def send_templated_email(self):
        current_site = Site.objects.get_current()

        ctx_dict = {
                'activation_key': self.activation_key,
                'expiration_days': up_settings.ACCOUNT_VERIFICATION_DAYS,
                'site': current_site
                }
        subject = ''.join(render_to_string(
            'userprofiles/mails/activation_email_subject.html',
            {'site': current_site}).splitlines())
        message = render_to_string('userprofiles/mails/activation_email.html', ctx_dict)
        send_mail(
                  message=message,
                  subject=subject,
                  from_email=settings.DEFAULT_FROM_EMAIL,
                  recipient_list=[self.user.email],
                  )
