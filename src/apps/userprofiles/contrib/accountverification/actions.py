# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.utils.translation import ugettext, gettext_lazy as _
from userprofiles.contrib.accountverification.models import AccountVerification


def resend_activation_email(modeladmin, request, queryset):

        rows_updated = 0
        for user in queryset.all():
            if user.is_active:
                modeladmin.message_user(request, ugettext("%(email)s already activated.") % {'email': user.email})
            else:
                try:
                    account_verification = AccountVerification.objects.get(user=user)
                except:
                    AccountVerification.objects.filter(user=user).delete()
                    account_verification = AccountVerification.objects.create_verification(user)
                account_verification.send_templated_email()
                rows_updated += 1

        if rows_updated == 1:
            message_bit = ugettext("1 record was")
        else:
            message_bit = ugettext("%(count)d records were") % {'count':rows_updated, }
        modeladmin.message_user(request, ugettext("%(msg)s successfully send activation email.") % {'msg':message_bit})

resend_activation_email.short_description = _("resend activation email for selected users")
