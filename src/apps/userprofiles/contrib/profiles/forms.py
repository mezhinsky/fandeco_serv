# -*- coding: utf-8 -*-
from django import forms
from django.utils.translation import ugettext_lazy as _

from userprofiles.settings import up_settings
from django.contrib.auth import get_user_model


class AvatarUploadForm(forms.Form):
    avatar = forms.ImageField()

    def __init__(self, *args, **kwargs):
        super(AvatarUploadForm, self).__init__(*args, **kwargs)
#        onchange = 'image_load("%s", "avatar_upload_form", "form-fields-send")' % (reverse_lazy('avatar_profile'))
#        self.fields['avatar'].widget.attrs = {'onchange':onchange, 'multiple':'' }

class ProfileForm(forms.ModelForm):
    first_name = forms.CharField(label=_('First name'), required=False)
    last_name = forms.CharField(label=_('Last name'), required=False)
    email = forms.EmailField(label=_('E-mail'))

    def __init__(self, *args, **kwargs):
        super(ProfileForm, self).__init__(*args, **kwargs)
        if not up_settings.PROFILE_ALLOW_EMAIL_CHANGE:
            del self.fields['email']
#        else:
#            self.fields.keyOrder.remove('email')
#            self.fields.keyOrder.insert(0, 'email')

        if not up_settings.REGISTRATION_FULLNAME:
            del self.fields['first_name']
            del self.fields['last_name']
#        else:
#            self.fields.keyOrder.remove('first_name')
#            self.fields.keyOrder.remove('last_name')
#            self.fields.keyOrder.insert(0, 'first_name')
#            self.fields.keyOrder.insert(1, 'last_name')

    def save(self, *args, **kwargs):
        obj = super(ProfileForm, self).save(*args, **kwargs)
        if up_settings.REGISTRATION_FULLNAME:
            obj.first_name = self.cleaned_data['first_name']
            obj.last_name = self.cleaned_data['last_name']

        if up_settings.PROFILE_ALLOW_EMAIL_CHANGE:
            obj.email = self.cleaned_data['email']

        if up_settings.REGISTRATION_FULLNAME or up_settings.PROFILE_ALLOW_EMAIL_CHANGE:
            obj.save()

        if hasattr(self, 'save_profile'):
            self.save_profile(obj, *args, **kwargs)

        return obj

    class Meta:
        model = get_user_model()
        exclude = ('avatar', 'password', 'date_joined', 'is_superuser', 'is_staff', 'is_active')

    class Media:
        css = {
               'all':('/static/assets/bootstrap/datepicker/css/bootstrap-datepicker.min.css',)
               }
        js = (
              '/static/assets/bootstrap/datepicker/js/bootstrap-datepicker.min.js',
              '/static/assets/bootstrap/datepicker/locales/bootstrap-datepicker.ru.min.js',
              )
