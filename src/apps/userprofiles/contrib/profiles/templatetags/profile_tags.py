from django import template
from userprofiles.contrib.profiles.forms import AvatarUploadForm

register = template.Library()

def user_avatar(context):
    request = context['request']
    return {
            'u_profile': request.user,
            'form':AvatarUploadForm()
        }


register.inclusion_tag(
    'userprofiles/tags/user_avatar.html',
    takes_context=True, name='user_avatar'
)(user_avatar)
