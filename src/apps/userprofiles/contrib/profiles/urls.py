from django.conf.urls import patterns, url


urlpatterns = patterns('userprofiles.contrib.profiles.views',
    url(r'^$', 'profile',
        name='userprofiles_profile'),
    url(r'^change/$', 'profile_change',
        name='userprofiles_profile_change'),
    url(r'^ajax-avatar/$', 'avatar_change',
        name='avatar_profile'),
)
