# -*- coding: utf-8 -*-
from django.db import models
from django.core.urlresolvers import reverse
from django.utils.translation import ugettext_lazy as _
from userprofiles.models import AbstractEmailUser


class User(AbstractEmailUser):
    """
    Concrete class of AbstractEmailUser.
    """
    GENDER_MALE = 'm'
    GENDER_FEMALE = 'f'

    GENDER = (
        (GENDER_MALE, _(u'Мужской')),
        (GENDER_FEMALE, _(u'Женский')),
    )
    avatar = models.ImageField(max_length=1024,
            upload_to='userprofiles/avatar/%Y/%m/%d', blank=True, null=True)
    gender = models.CharField(u"Пол", choices=GENDER, max_length=1,
                              blank=True, null=True)
    birthdate = models.DateField(u"День рождения", null=True, blank=True)

    ga_hash_key = models.CharField(_('Google Analitics key'), max_length=40, blank=True)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    class Meta(AbstractEmailUser.Meta):
        abstract = False
        swappable = 'AUTH_USER_MODEL'

    def __unicode__(self):
        return u"{0} / {1}".format(self.email, self.get_full_name())

    def get_absolute_url(self):
        return reverse('userprofiles_profile')


