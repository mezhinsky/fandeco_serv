# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import userprofiles.contrib.emailverification.models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='EmailVerification',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('old_email', models.EmailField(max_length=254, verbose_name='Old e-mail address')),
                ('new_email', models.EmailField(max_length=254, verbose_name='New e-mail address')),
                ('token', models.CharField(default=userprofiles.contrib.emailverification.models.generate_token, max_length=40, verbose_name='Token')),
                ('code', models.CharField(default=userprofiles.contrib.emailverification.models.generate_token, max_length=40, verbose_name='Code')),
                ('is_approved', models.BooleanField(default=False, verbose_name='Approved')),
                ('is_expired', models.BooleanField(default=False, verbose_name='Expired')),
                ('expiration_date', models.DateTimeField(default=userprofiles.contrib.emailverification.models.generate_confirm_expire_date, verbose_name='Expiration date')),
                ('user', models.ForeignKey(verbose_name='User', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': 'E-mail verification',
                'verbose_name_plural': 'E-mail verifications',
            },
        ),
    ]
