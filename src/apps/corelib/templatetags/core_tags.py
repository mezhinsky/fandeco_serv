# coding: utf-8
from django import template
from django.contrib.contenttypes.models import ContentType
from django.core.cache import cache

register = template.Library()


@register.filter
def get_content_type(obj):
    cache_key = 'CONTENT_TYPE_%s' % obj.pk
    ctype = cache.get(cache_key)
    if not ctype:
        ctype_object = ContentType.objects.get_for_model(type(obj),
                                              for_concrete_model=False)
        ctype = ctype_object.pk
        cache.set(cache_key, ctype)
        
    return ctype
