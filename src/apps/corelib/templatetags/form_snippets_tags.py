# coding: utf-8
from django import template
from django.utils.safestring import mark_safe
from django.forms.forms import BoundField
from django.utils.encoding import force_text

register = template.Library()


@register.filter
def field_type(field, ftype):
    try:
        t = field.field.widget.__class__.__name__
        return t.lower() == ftype
    except:
        pass
    return False


@register.filter
def get_field_type(field):
    try:
        t = field.field.widget.__class__.__name__
        return t.lower()
    except:
        pass
    return False


@register.simple_tag
def form_field(form, pattern, value, classes=None):
    field = form[pattern % value]
    if classes:
        return css_class(field, classes)
    else:
        return field


@register.assignment_tag
def get_form_field(form, pattern, value):
    return form[pattern % value]


def get_attrs(field):
    try:
        attrs = field.field.widget.attrs
    except:
        attrs = {}
    return attrs


def set_attrs(field, attrs):
    field.field.widget.attrs = attrs
    return field


def get_class_value(field):
    attrs = get_attrs(field)
    return attrs.get('class', '')


def form_field_attr(field, attr, value):
    if isinstance(field, BoundField):
        attrs = get_attrs(field)
        attrs[attr] = value
        field.field.widget.attrs = attrs
    return field


def append_class(field, value):
    attrs = get_attrs(field)
    attrs['class'] = u"%s %s" % (get_class_value(field), value)
    field = set_attrs(field, attrs)
    return field


def field_attr(field, arg):
    name = arg.split(',')[0]
    return form_field_attr(field, name, arg[len(name) + 1:])
register.filter('field_attr', field_attr)


def tabindex(value, arg):
    return form_field_attr(value, 'tabindex', arg)
register.filter('tabindex', tabindex)


def css_class(value, arg):
    # print value
    field = form_field_attr(value, 'class', arg)
    if isinstance(value, BoundField):
        # добавление класса form-disabled для readonly-полей
        if get_attrs(value).get('readonly', False):
            field = append_class(value, 'form-disabled')
    return field
register.filter('css_class', css_class)


def err_class(value, arg):
    if value and value.errors:
        return append_class(value, arg)
    return value
register.filter('err_class', err_class)


def placeholder(value, arg=None):
    if arg is None:
        arg = value.label
    return form_field_attr(value, 'placeholder', arg)
register.filter('placeholder', placeholder)


@register.filter
def error_list(form):
    res = u''
    if len(form.errors):
        for key, errors in form.errors.items():
            err = u', '.join([force_text(e) for e in errors])
            if key in form.fields:
                res += u'<li>%s: %s</li>' % (form.fields[key].label, err)
            else:
                res += u'<li>%s</li>' % err
        res = u'<ul>%s</ul>' % res
    return mark_safe(res)


@register.filter
def error_list_without_fields_errors(form, css_class=''):
    res = u''
    if form and len(form.errors):
        for key, errors in form.errors.items():
            err = u', '.join([force_text(e) for e in errors])
            if key not in form.fields:
                res += u'<li>%s</li>' % err
        if len(res):
            if css_class:
                css_class = u' class="{0}"'.format(css_class)
            res = u"<div{0}><ul>{1}</ul></div>".format(css_class, res)
    return mark_safe(res)


@register.filter
def field_error_list(field, css_class='error-list'):
    s = ''
    res = ''
    if isinstance(field, BoundField) and len(field.errors):
        for err in field.errors:
            s += u'<li>%s</li>' % (err,)
        if css_class:
            css_class = u' class="{0}"'.format(css_class)
        res = u"<div{0}><div><i></i><ul>{1}</ul></div></div>".format(css_class, s)
    return mark_safe(res)
