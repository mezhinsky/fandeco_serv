# -*- coding: utf-8 -*-
from django.http import HttpResponse
from django.template.loader import render_to_string
import json


class JsonViewMixin():
    def render_to_response(self, context, **response_kwargs):
        #html = super(JsonViewMixin, self).render_to_response(context, **response_kwargs)
        html = render_to_string(self.get_template_names()[0], context)
        json_dump = json.dumps({'html': html})
        return HttpResponse(json_dump, status=200)