from django.views.generic import DetailView, TemplateView
from django.core.paginator import InvalidPage
from django.utils.translation import ugettext_lazy as _

from .models import Price


class CatalogueView(TemplateView):
    """
    Browse all products in the catalogue
    """
    context_object_name = "products"
    template_name = 'catalogue/browse.html'


class PriceListView(TemplateView):
    """
    Browse all products in the catalogue
    """
    context_object_name = "products"
    template_name = 'catalogue/pirce_list.html'

    def get_context_data(self, **kwargs):
        context = super(PriceListView, self).get_context_data(**kwargs)
        context['products'] = Price.objects.all()
        return context