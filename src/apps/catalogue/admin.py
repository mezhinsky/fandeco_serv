from django.contrib import admin

from .models import Product, ProductClass, Price


class ProductAdmin(admin.ModelAdmin):
    pass

class ProductClassAdmin(admin.ModelAdmin):
    pass

class PriceAdmin(admin.ModelAdmin):
    pass

MODELS = {
    Product: ProductAdmin,  
    ProductClass: ProductClassAdmin,  
    Price: PriceAdmin,  
}

for model_or_iterable, admin_class in MODELS.items():
    admin.site.register(model_or_iterable, admin_class)