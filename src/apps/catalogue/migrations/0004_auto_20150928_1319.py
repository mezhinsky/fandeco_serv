# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalogue', '0003_auto_20150928_1317'),
    ]

    operations = [
        migrations.AlterField(
            model_name='productclass',
            name='slug',
            field=models.SlugField(unique=True, verbose_name='Slug'),
        ),
    ]
