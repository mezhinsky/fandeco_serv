from django.db import models
from django.utils.encoding import python_2_unicode_compatible
from django.utils.translation import ugettext_lazy as _, pgettext_lazy

from autoslug import AutoSlugField

@python_2_unicode_compatible
class ProductClass(models.Model):
    name = models.CharField(_('Name'), max_length=128)
    slug = models.SlugField(_('Slug'), max_length=50, unique=True)
    requires_shipping = models.BooleanField(_("Requires shipping?"),
                                            default=True)

    class Meta:
        abstract = False
        app_label = 'catalogue'
        ordering = ['name']
        verbose_name = _("Product class")
        verbose_name_plural = _("Product classes")

    def __str__(self):
        return self.name

    def __unicode__(self):
        return self.name


@python_2_unicode_compatible
class Product(models.Model):
    title = models.CharField(_(u'Product title'),
                             max_length=255, blank=True)
    product_class = models.ForeignKey(
        'ProductClass', null=True, blank=True, on_delete=models.PROTECT,
        verbose_name=_('Product type'), related_name="products",
        help_text=_("Choose what type of product this is"))

    class Meta:
        abstract = False
        app_label = 'catalogue'
        ordering = ['title']
        verbose_name = _("Product")
        verbose_name_plural = _("Products")

    def __str__(self):
        return self.title

    def __unicode__(self):
        return self.title


@python_2_unicode_compatible
class Price(models.Model):
    product = models.ForeignKey('Product', verbose_name=_("Product"))
    company = models.ForeignKey('company.Company', verbose_name=_("Company"))
    #cost = models.CharField(_(u'Product title'),
    #                         max_length=255, blank=True)


    def get_titile(self):
        return self.product.title

    class Meta:
        abstract = False
        app_label = 'catalogue'
        ordering = ['product']
        verbose_name = _("Pirce")
        verbose_name_plural = _("Pirces")

    def __str__(self):
        return self.product.title

    def __unicode__(self):
        return self.product.title