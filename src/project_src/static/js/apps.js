$.ajaxSetup({
     beforeSend: function(xhr, settings) {
         function getCookie(name) {
             var cookieValue = null;
             if (document.cookie && document.cookie != '') {
                 var cookies = document.cookie.split(';');
                 for (var i = 0; i < cookies.length; i++) {
                     var cookie = jQuery.trim(cookies[i]);
                     // Does this cookie string begin with the name we want?
                 if (cookie.substring(0, name.length + 1) == (name + '=')) {
                     cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                     break;
                 }
             }
         }
         return cookieValue;
         }
         if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
             // Only send the token to relative URLs i.e. locally.
             xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
         }
     }
});

$.fn.classFade = function(cls, time){
	var t = this
	t.addClass(cls)
	setTimeout(function(){t.removeClass(cls)}, time)
	return t
}

function avatar_load(){
    $('#avatar_loading').css('visibility', 'visible');
    var formData = new FormData($('#avatar_upload_form')[0]);
    $.ajax({
        type: 'POST',
        url: $('#avatar_upload_form').attr('action'),
        data: formData,
        dataType:'json',
        success: function(result) {
        $('.js-user-avatar').html(
            result['html']
            );
        },
    	complete:function(){
    		$('#avatar_loading').css('visibility', 'hidden');
    	},
    	 cache: false,
         contentType: false,
         processData: false
      }
    );
    }

function update_cart(){
	$.getJSON('/cart/', function(j){
		$('.user-cart .list').html(j['html'])
	})
}

function myTrim(x) {
    return x.replace(/^\s+|\s+$/gm,'');
}

$(document).ready(function(){
   $(".megamenu").megamenu();
   $('.js-avatar-load input').change(avatar_load);
   if($.fn.datepicker)
	   $('.date').datepicker({
		    endDate: "today",
		    language: "ru",
		    autoclose: true
		});
   
   /* Functions for edit list of items using AJAX */
   $(document).on('click','.item-add, .item-edit', function(){
	   var modal = $($(this).attr('data-target'))
	   modal.find('#'+modal.attr('aria-labelledby')).text($(this).attr('data-header'))
	   modal.find('form').attr({
		   'action': $(this).attr('data-url'),
		   'data-item': this.id||""
	   }).find('.errorlist').remove()
   })
   $(document).on('click','.item-edit', function(){
	   var modal = $($(this).attr('data-target'))
	   modal.find('form').find('[name='+$(this).attr('data-field')+']').val($(this).attr('data-name'))
	   modal.modal('show')
   })
   $(document).on('click','.item-add', function(){
	   var modal = $($(this).attr('data-target'))
	   modal.find('form')[0].reset()
	   modal.modal('show')
   })
   
   $(document).on('click','.list-edit-dialog .btn-primary', function(){
	   var modal = $('.list-edit-dialog'), form = modal.find('form'), result = $(form.attr('data-result')), item = form.attr('data-item')
	   
	   $.ajax({
		 url:form.attr('action'),
		 type:'POST',
		 dataType:'json',
		 data:form.serialize(),
		 success:function(data){
			 if(!data['errors']){
				if(item){
					 $('#'+item).parents('.list-item').replaceWith(data['html'])
				}
				else
					result.append(data['html'])
				result.find('.list-empty').addClass('hidden')
				modal.modal('hide')
			 }else
				 form.replaceWith(data['html'])
		 }
	   })
	   
	   
   })
   
   $(document).on('click','.item-remove', function(){
	   var t = $(this), list = t.parents('.items-list').eq(0)
	   $.post(t.attr('data-url'), function(){
		   t.parents('.list-item').remove()
		   if(list.find('.list-item').length == 0)
			   list.find('.list-empty').removeClass('hidden')
		   if(t.attr('data-callback')){
			   f = function(){eval(t.attr('data-callback'))}
			   f.apply(t[0])
		   }
	   })
   })
   
   /* links to AJAX views */
   $(document).on('click','.js-ajax-link', function(){
	   var t = $(this), result = $(t.attr('data-result'))
	   $.ajax({
		   url:t.attr('data-url'),
		   type:t.attr('data-method')||'GET',
		   dataType:'json',
		   success:function(data){
			   result[t.attr('data-replace')?'replaceWith':'html'](data['html'])
			   if(t.attr('data-callback')){
				   f = function(){eval(t.attr('data-callback'))}
				   f.apply(t[0])
			   }
		   }
	   })
   })
   
   /* AJAX forms */
   $(document).on('submit','.js-ajax-form', function(){
	   var t = $(this), result = t.attr('data-result')
	   $.ajax({
		 url:t.attr('data-action'),
		 type:t.attr('data-method')||'GET',
		 data: t.serialize(),
		 dataType:'json',
		 success:function(data){
			 $.each(result.split('|'), function(i,v){
				 $(v).html(data['html'].split('|')[i]) 
			 })
			 if(t.attr('data-callback')){
				 f = function(){eval(t.attr('data-callback'))}
				 f.apply(t[0])
			 }
		 }
	   })
   })
});