
from django.conf.urls import patterns, include, url
from django.conf import settings
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.http import HttpResponseServerError
from django.template.loader import render_to_string
from django.template.context import RequestContext
from django.views.generic.base import TemplateView, RedirectView

import autocomplete_light
autocomplete_light.autodiscover()

from django.contrib import admin as default_admin
default_admin.autodiscover()

urlpatterns = patterns('',
    url(r'^$', TemplateView.as_view(template_name="homepage.html",
            get_context_data=lambda: {'is_homepage': True}), name='home_page'),
    url(r'^autocomplete/', include('autocomplete_light.urls')),
    url(r'^accounts/', include('userprofiles.urls')),
    url(r'^captcha/', include('captcha.urls')),
    url(r'^ckeditor/', include('ckeditor.urls')),
    url(r'^contacts/', include('contact_form.urls')),
    url(r'^pages/', include('pages.urls')),
    # url(r'^robots.txt$', include('robots.urls')),
    # for calendar
    # url(r'^area101/jsi18n/', 'django.views.i18n.javascript_catalog'),
    url(r'^admin/', include(default_admin.site.urls)),
)

def error500(request, template_name='500.html'):
    """ Error 500 handler """
    try:
        output = render_to_string(template_name, {}, RequestContext(request))
    except:
        output = "Critical error. Administrator was notified."
    return HttpResponseServerError(output)

# forcing user to configure httpd
if settings.DEBUG:
    if 'rosetta' in settings.INSTALLED_APPS:
        urlpatterns += patterns('',
            url(r'^rosetta/', include('rosetta.urls')),
        )
    if 'debug_toolbar' in settings.INSTALLED_APPS:
        import debug_toolbar
        urlpatterns += patterns('',
            url(r'^__debug__/', include(debug_toolbar.urls)),
        )
    urlpatterns += patterns('',
        url(r'^favicon\.ico$', RedirectView.as_view(url='/static/images/favicon.ico'), name='favicon'),
        url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {
                'document_root': settings.MEDIA_ROOT,
        }),
    )
    urlpatterns += staticfiles_urlpatterns()

urlpatterns += patterns('', url(r'^', include('bshop.urls')),)
