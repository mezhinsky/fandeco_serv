# -*- coding: utf-8 -*-
from base import *

DEBUG = True

TEMPLATES[0]['OPTIONS']['debug'] = True  # for template debug
TEMPLATES[0]['OPTIONS']['context_processors'].insert(0, 'django.core.context_processors.debug')

SENTRY_CLIENT = 'raven.contrib.django.raven_compat.DjangoClient'
RAVEN_CONFIG = {
    'dsn': '',
    'register_signals': True,
}

INSTALLED_APPS += (
    'raven.contrib.django.raven_compat',
)
