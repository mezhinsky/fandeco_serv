# -*- coding: utf-8 -*-

import os
import sys

DEBUG = False
ALLOWED_HOSTS = []

BASE_DIR = os.path.dirname(os.path.dirname(__file__))
PROJECT_ROOT = BASE_DIR  # Корневая папка project_src
MANAGE_ROOT = os.path.dirname(PROJECT_ROOT)  # Корневая папка manage.py

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.8/howto/deployment/checklist/
join_to_project = lambda slug: os.path.join(PROJECT_ROOT, slug)
join_to_manage = lambda slug: os.path.join(MANAGE_ROOT, slug)

# Путь к временной папке проекта
PROJECT_TMP_ROOT = join_to_manage('tmp')  # свое
PROJECT_LOGS_ROOT = join_to_manage('logs')  # свое

sys.path.insert(0, join_to_manage('apps'))
sys.path.insert(1, join_to_manage('compat'))
sys.path.insert(2, MANAGE_ROOT)


# Application definition

INSTALLED_APPS = (

    # require to begin
    'django_su',
    'admin_tools',
    'admin_tools.theming',
    'admin_tools.menu',
    'admin_tools.dashboard',
    # 'flat',

    # django
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.sitemaps',
    'django.contrib.sites',

    # 3rd-party
    'captcha',
    'endless_pagination',
    'pytils',
    # "post_office",
    'sorl.thumbnail',
    'static_sitemaps',
    'treebeard',

    # compat
    'ckeditor',
    'seotools',

    # apps
    # 'apps.company',
    # 'apps.catalogue',
    'bshop.contrib.cart',
    'bshop.contrib.catalog',
    'bshop.contrib.wishlists',
    'contact_form',
    'corelib',
    'pages',
    'slider',
    'userprofiles',
    'userprofiles.contrib.accountverification',
    'userprofiles.contrib.emailverification',
    'userprofiles.contrib.profiles',

    # require to end
    'autocomplete_light',
    'django.contrib.admin',
    # 'rest_framework.authtoken',
    'rest_framework',
    'rest_framework_swagger',
)

AUTH_USER_MODEL = 'profiles.User'
USERPROFILES_USE_EMAIL_VERIFICATION = True


TEST_RUNNER = 'project_src.test_runner.TeamcityTestSuiteRunner'
TEST_EXCLUDE = [
    # удалять по мере написания проходящих тестов
    'bshop.contrib.cart',
    'bshop.contrib.catalog',
    'bshop.contrib.wishlists',
    'userprofiles',
    'userprofiles.contrib.accountverification',
    'userprofiles.contrib.emailverification',
    'userprofiles.contrib.profiles',


    # точно не надо тестировать
    "admin_tools",
    "autocomplete_light",
    'captcha',
    'endless_pagination',
    'sorl.thumbnail',
    'pytils',
    "post_office",
    'treebeard',
    'rest_framework.authtoken',
    'rest_framework',
    'rest_framework_swagger',
]
COVERAGE_MODULE_EXCLUDES = TEST_EXCLUDE
COVERAGE_PATH_EXCLUDES = [
    r'migrations',
]
COVERAGE_REPORT_HTML_OUTPUT_DIR = '.coverage'


ADMINS = (
    ('Berg', 'testing.berg@gmail.com'),
)

MANAGERS = (
    ('Berg', 'testing.berg@gmail.com'),
)

DEFAULT_FROM_EMAIL = 'testing.berg@gmail.com'
SERVER_EMAIL = 'testing.berg@gmail.com'
EMAIL_SUBJECT_PREFIX = '[FanDeco] '

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.middleware.security.SecurityMiddleware',
    'bshop.contrib.cart.middleware.CartMiddleware',
)

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
            # insert your TEMPLATE_DIRS here
            join_to_project('templates'),
        ],
        # 'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.core.context_processors.request',
                'django.contrib.messages.context_processors.messages',
                'django.core.context_processors.static',
                'django.core.context_processors.media',
            ],
            'loaders': [
                # insert your TEMPLATE_LOADERS here
                'django.template.loaders.filesystem.Loader',
                'django.template.loaders.app_directories.Loader',
                # 'django.template.loaders.eggs.Loader',
                # "admin_tools.template_loaders.Loader",
            ],
        },
    },
]

ROOT_URLCONF = 'project_src.urls'
WSGI_APPLICATION = 'project_src.wsgi.application'

# Database
# https://docs.djangoproject.com/en/1.8/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': join_to_project('db.sqlite3'),
    }
}

DATABASE_OPTIONS = 'charset=utf8'
DEFAULT_CHARSET = 'utf-8'


LANGUAGE_CODE = 'ru'
LOCALE_PATHS = (
    join_to_project('locale'),
)

SITE_ID = 1
USE_I18N = True
USE_L10N = True
USE_TZ = True
TIME_ZONE = 'UTC'


STATICFILES_DIRS = (
    join_to_project('static'),
)

STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
)

MEDIA_ROOT = join_to_manage('media')
STATIC_ROOT = join_to_manage('staticfiles')
MEDIA_URL = '/media/'
STATIC_URL = '/static/'


BASE_DOMAIN = 'homister.ru'
MAIN_SITE = 'homister'


# FORMATS
USE_THOUSAND_SEPARATOR = True
THOUSAND_SEPARATOR = ' '
NUMBER_GROUPING = 3

DATE_FORMAT = u'j F Y р.'
TIME_FORMAT = 'G:i:s'
DATETIME_FORMAT = u'j E Y р. H:i:s'
YEAR_MONTH_FORMAT = u'F Y р.'
MONTH_DAY_FORMAT = 'j F'
SHORT_DATE_FORMAT = 'd.m.Y'
SHORT_DATETIME_FORMAT = 'd.m.Y H:i'
SHORT_DATETIME_SEC_FORMAT = 'd.m.Y H:i:s'
FIRST_DAY_OF_WEEK = 1  # Monday
DATE_INPUT_FORMATS = (
    '%d.%m.%Y',  # '25.10.2006'
    '%d.%m.%y',  # '25.10.06'
    '%Y-%m-%d',  # '2006-10-25'
)
TIME_INPUT_FORMATS = (
    '%H:%M:%S',  # '14:30:59'
    '%H:%M',  # '14:30'
)
DATETIME_INPUT_FORMATS = (
    '%d.%m.%Y %H:%M:%S',  # '25.10.2006 14:30:59'
    '%d.%m.%Y %H:%M',  # '25.10.2006 14:30'
    '%d.%m.%Y',  # '25.10.2006'
    '%d.%m.%y %H:%M:%S',  # '25.10.06 14:30:59'
    '%d.%m.%y %H:%M',  # '25.10.06 14:30'
    '%d.%m.%y',  # '25.10.06'
    '%Y-%m-%d %H:%M:%S',  # '2006-10-25 14:30:59'
    '%Y-%m-%d %H:%M',  # '2006-10-25 14:30'
    '%Y-%m-%d',  # '2006-10-25'
)


STATICSITEMAPS_ROOT_DIR = os.path.join(MEDIA_ROOT, 'sitemaps')
STATICSITEMAPS_ROOT_SITEMAP = 'project_src.sitemaps.sitemaps'
STATICSITEMAPS_REFRESH_AFTER = 360  # defaults to 60 ~ 1 hour
STATICSITEMAPS_URL = os.path.join(MEDIA_URL, 'sitemaps')

LOGGING = {
    'version': 1,
    'disable_existing_loggers': True,
    'root': {
        'level': 'WARNING',
        'handlers': ['sentry'],
    },
    'formatters': {
        'verbose': {
            'format': '%(levelname)s %(asctime)s %(module)s %(process)d %(thread)d %(message)s'
        },
    },
    'handlers': {
        'sentry': {
            'level': 'ERROR',
            'class': 'raven.contrib.django.raven_compat.handlers.SentryHandler',
        },
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'verbose'
        }
    },
    'loggers': {
        'django.db.backends': {
            'level': 'ERROR',
            'handlers': ['console'],
            'propagate': False,
        },
        'raven': {
            'level': 'DEBUG',
            'handlers': ['console'],
            'propagate': False,
        },
        'sentry.errors': {
            'level': 'DEBUG',
            'handlers': ['console'],
            'propagate': False,
        },
    },
}

try:
    from .apps import *
except ImportError:
    import sys
    sys.stderr.write('Unable to read settings in apps.py\n')
    DEBUG = False


# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'rrwbpwy3%7ig@=ohey8$)53o)@a$1#*!3dqda)9@3a%6@7ak0a'
