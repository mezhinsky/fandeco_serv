# -*- coding: utf-8 -*-
# Django local settings for project.
from .develop import *

DEBUG = True
TEMPLATES[0]['OPTIONS']['debug'] = True  # for template debug
TESTING_MODE = len(sys.argv) > 1 and sys.argv[1] == 'test'

# THUMBNAIL_DEBUG = True

EMAIL_SUBJECT_PREFIX = '[Local] '
EMAIL_BACKEND = 'django.core.mail.backends.filebased.EmailBackend'
EMAIL_FILE_PATH = join_to_manage('emails')

"""
CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.dummy.DummyCache',
    }
}
"""

DATABASES = {
    'default': {
        # Add backend 'postgresql_psycopg2', 'postgresql', 'mysql', 'sqlite3' or 'oracle'.
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'fandeco',
        'USER': 'fandeco',  # Not used with sqlite3.
        'PASSWORD': 'fandeco',  # Not used with sqlite3.
        'HOST': 'localhost',  # Set to empty string for localhost. Not used with sqlite3.
        'PORT': '',  # Set to empty string for default. Not used with sqlite3.
    },
}

if TESTING_MODE:
    EMAIL_BACKEND = 'django.core.mail.backends.locmem.EmailBackend'
    INSTALLED_APPS += (
        'test_without_migrations',
    )
else:
    MIDDLEWARE_CLASSES += (
            'debug_toolbar.middleware.DebugToolbarMiddleware',
    )
    INSTALLED_APPS += (
            'debug_toolbar',
            'rosetta',
    )

    '''
        Rosetta configuration
    '''
    ROSETTA_MESSAGES_PER_PAGE = 20
    ENABLE_TRANSLATION_SUGGESTIONS = True
    ROSETTA_ENABLE_TRANSLATION_SUGGESTIONS = True
    YANDEX_TRANSLATE_KEY = "trnsl.1.1.20131227T134851Z.3d9887e6554b4b80.1a22e934387cf73c99c1686b29c8504bb320c55e"
    ROSETTA_WSGI_AUTO_RELOAD = True
    ROSETTA_UWSGI_AUTO_RELOAD = True
    ROSETTA_STORAGE_CLASS = 'rosetta.storage.CacheRosettaStorage'

    '''
        django-debug-toolbar
    '''
    DEBUG_TOOLBAR_PATCH_SETTINGS = False
    DEBUG_TOOLBAR_PANELS = [
        'debug_toolbar.panels.versions.VersionsPanel',
        'debug_toolbar.panels.timer.TimerPanel',
        'debug_toolbar.panels.settings.SettingsPanel',
        'debug_toolbar.panels.headers.HeadersPanel',
        'debug_toolbar.panels.request.RequestPanel',
        'debug_toolbar.panels.sql.SQLPanel',
        'debug_toolbar.panels.staticfiles.StaticFilesPanel',
        'debug_toolbar.panels.templates.TemplatesPanel',
        'debug_toolbar.panels.cache.CachePanel',
        'debug_toolbar.panels.signals.SignalsPanel',
        'debug_toolbar.panels.logging.LoggingPanel',
        'debug_toolbar.panels.redirects.RedirectsPanel',
    ]

    # This example is unlikely to be appropriate for your project.
    CONFIG_DEFAULTS = {
        # Toolbar options
        'RESULTS_STORE_SIZE': 3,
        'SHOW_COLLAPSED': True,
        # Panel options
        'INTERCEPT_REDIRECTS': True,
        'SQL_WARNING_THRESHOLD': 100,  # milliseconds
    }

    DEBUG_TOOLBAR_CONFIG = {
        'INTERCEPT_REDIRECTS': False,
    }


LOGGING = {
    'version': 1,
    'disable_existing_loggers': True,
    'root': {
        'level': 'DEBUG' if DEBUG else 'WARNING',  # {'level': 'DEBUG' if DEBUG else 'INFO'},
        'handlers': ['console', ],
    },
    'formatters': {
        'verbose': {
            'format': '%(levelname)s %(asctime)s %(module)s %(process)d %(thread)d %(message)s'
        },
    },
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse',
        }
    },
    'handlers': {
        'console': {
            'level': 'INFO',
            'class': 'logging.StreamHandler',
            'stream': sys.stdout,
            # 'formatter': 'verbose'
        }
    },
    'loggers': {
        'events.views': {
            'handlers': ['console'],
            'level': 'DEBUG',
        },
        'raven': {
            'level': 'DEBUG',
            'handlers': ['console'],
            'propagate': False,
        },
        'sentry.errors': {
            'level': 'DEBUG',
            'handlers': ['console'],
            'propagate': False,
        },

    },
}