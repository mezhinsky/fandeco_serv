from django.test.runner import DiscoverRunner
import unittest
from unittest.loader import defaultTestLoader

from django.conf import settings

EXCLUDED_APPS = tuple(getattr(settings, 'TEST_EXCLUDE', []))


class TeamcityTestSuiteRunner(DiscoverRunner):

    def build_suite(self, test_labels, extra_tests=None, **kwargs):
        """
        from http://www.mavenrd.com/blog/django-testing-unchained/
        """
        suite = None
        if test_labels:
            suite = defaultTestLoader.loadTestsFromNames(test_labels)
        else:
            test_labels = [app for app in settings.INSTALLED_APPS if not app.startswith(EXCLUDED_APPS)]
            suite = super(TeamcityTestSuiteRunner, self).build_suite(test_labels)
        return suite
